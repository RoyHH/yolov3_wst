from matplotlib import pyplot as plt
from matplotlib.pyplot import MultipleLocator
from mpl_toolkits.mplot3d import Axes3D
import cv2 as cv
import numpy as np
from openpyxl import Workbook
from openpyxl import load_workbook
import pandas as pd

def get_data(lines, epoch_num, epoch_size, check_num, wst_check_w, key_w):
    for i, line in enumerate(lines):
        line = line.strip()
        if line.startswith('*'):
            line = line.strip('*')
            line = line.strip()
            # print(line)
            epoch_num = 300 - epoch_size
            epoch_size = epoch_size - 1

        elif line.startswith('#'):
            line = line.rstrip('=')
            line = line.strip()
            line = line.strip('#')
            line = line.lstrip('[')
            line = line.rstrip(']')
            # print(line)
            check_num = 1 if line == key_w else 0

        else:
            a = i % 21
            row_n = (a - 12) if a > 11 else (a - 2)
            for j, data in enumerate(line.split()):
                wst_check_w[epoch_num, check_num, row_n, j] = data

    return wst_check_w

def show_data(wst_check_w_10, out_path_10, zd=None):
    fig1 = plt.figure(figsize=(20, 20))
    ax1 = plt.axes(projection='3d')
    X = np.arange(0, 60, 1)
    Y = np.arange(0, 81, 1)
    w_t, w_id = np.meshgrid(X, Y)
    w_data = wst_check_w_10[w_t, w_id]

    ax1.plot_surface(w_t, w_id, w_data, rstride=1, cstride=1, alpha=0.3, cmap='rainbow')  # 生成表面， alpha 用于控制透明度
    ax1.contour(w_t, w_id, w_data, zdir='z', offset=200, cmap="rainbow")  # 生成z方向投影，投到x-y平面
    # ax1.contour(w_t, w_id, w_data, zdir='x', offset=-6, cmap="rainbow")  # 生成x方向投影，投到y-z平面
    # ax1.contour(w_t, w_id, w_data, zdir='y', offset=6, cmap="rainbow")  # 生成y方向投影，投到x-z平面
    # ax1.contourf(w_t, w_id, w_data, zdir='y', offset=6, cmap="rainbow")   #生成y方向投影填充，投到x-z平面，contourf()函数

    # 设定坐标轴刻度
    x_major_locator = MultipleLocator(1)
    ax1.xaxis.set_major_locator(x_major_locator)
    y_major_locator = MultipleLocator(1)
    ax1.yaxis.set_major_locator(y_major_locator)

    # 设定坐标轴字体字号
    plt.tick_params(labelsize=5)
    labels = ax1.get_xticklabels() + ax1.get_yticklabels()
    [label.set_fontname('Times New Roman') for label in labels]

    # 设定坐标轴范围
    ax1.set_xlabel('t')
    ax1.set_xlim(0, 61)  # 拉开坐标轴范围显示投影
    ax1.set_ylabel('id')
    ax1.set_ylim(0, 82)
    ax1.set_zlabel('data')
    ax1.set_zlim(0, zd)

    plt.savefig(fname=out_path_10, figsize=[20, 20])

    # plt.legend()
    plt.show()

def write_excel(data_01_exl, sheet_01):
    [r, c] = data_01_exl.shape  # h为行数，l为列数
    for i in range(r):
        row = []
        for j in range(c):
            row.append(data_01_exl[i, j])
        sheet_01.append(row)

def data_to_excel(data_01, sheet_01, add_column):
    data_01_9 = np.reshape(data_01, (300, 9, 9))

    for i in range(300):
        if i == 0:
            data_01_exl = np.column_stack((data_01_9[i, :, :], add_column))
        else:
            data_01_exl = np.append(data_01_exl, np.column_stack((data_01_9[i, :, :], add_column)), axis=1)
    print("wst_check_wf_01_exl =>", data_01_exl)

    write_excel(data_01_exl, sheet_01)

def save_to_excel(excel_out_path, data_01, data_10, add_column):
    df = pd.DataFrame()                     # 创建excel文档，只是一个圆括号的话说明创建的数据表是空的
    df.to_excel(excel_out_path)             # 调用to_excel 方法
    # print('Done!')

    wb = Workbook()
    print(wb.get_sheet_names())             # 提供一个默认名叫Sheet的表，office2016下新建提供默认Sheet1

    sheet_01 = wb.get_sheet_by_name('Sheet')
    sheet_01.title = 'check_w_01'           # 直接赋值就可以改工作表的名称
    data_to_excel(data_01, sheet_01, add_column)

    wb.create_sheet('check_w_10', index=0)  # 被安排到第二个工作表，index=0就是第一个位置
    sheet_10 = wb.get_sheet_by_name('check_w_10')
    data_to_excel(data_10, sheet_10, add_column)

    wb.save(excel_out_path)



if __name__ == '__main__':

    '''================================ 处理文件 + 绘图 ================================'''
    epoch_num = 0
    epoch_size = 300
    check_num = 0

    '''*********** 处理 wst_check_w 文件 ***********'''
    path = "../wst_without_foreign_check/wst_check_w.txt"
    out_path_01 = "../wst_without_foreign_check/wst_check_w_01.png"
    out_path_10 = "../wst_without_foreign_check/wst_check_w_10.png"
    key_w = 'wst_check_w_10'

    with open(path, 'r') as f:
        lines = f.readlines()
        # print(lines)

    wst_check_w = np.zeros((300, 2, 9, 9), dtype=int)
    wst_check_w = get_data(lines, epoch_num, epoch_size, check_num, wst_check_w, key_w)
    # print(wst_check_w[4, 1, 4, 2])
    wst_check_w_01 = wst_check_w[:, 0, :, :]
    wst_check_w_10 = wst_check_w[:, 1, :, :]

    # 绘图
    wst_check_w_01_81 = np.reshape(wst_check_w_01, (-1, 81))
    show_data(wst_check_w_01_81, out_path_01, zd=20000)
    wst_check_w_10_81 = np.reshape(wst_check_w_10, (-1, 81))
    show_data(wst_check_w_10_81, out_path_10, zd=20000)

    '''*********** 处理 wst_check_w_% 文件 ***********'''
    pathf = "../wst_without_foreign_check/wst_check_w%.txt"
    out_path_01f = "../wst_without_foreign_check/wst_check_wf_01.png"
    out_path_10f = "../wst_without_foreign_check/wst_check_wf_10.png"
    key_wf = 'wst_check_1'

    with open(pathf, 'r') as f:
        linesf = f.readlines()
        # print(lines)

    wst_check_wf = np.zeros((300, 2, 9, 9), dtype=float)
    wst_check_wf = get_data(linesf, epoch_num, epoch_size, check_num, wst_check_wf, key_wf)
    # print(wst_check_wf[4, 1, 4, 2])
    wst_check_wf_01 = wst_check_wf[:, 0, :, :]
    wst_check_wf_10 = wst_check_wf[:, 1, :, :]

    # 绘图
    wst_check_wf_01_81 = np.reshape(wst_check_wf_01, (-1, 81))
    show_data(wst_check_wf_01_81, out_path_01f, zd=1)
    wst_check_wf_10_81 = np.reshape(wst_check_wf_10, (-1, 81))
    show_data(wst_check_wf_10_81, out_path_10f, zd=1)


    '''================================ 转存为excel 供origin绘图 ================================'''
    add_column = np.zeros(9)

    '''*********** wst_check_w 文件转存为excel文件 ***********'''
    excel_out_path = "../wst_without_foreign_check/wst_check_w.xlsx"
    save_to_excel(excel_out_path, wst_check_w_01, wst_check_w_10, add_column)

    '''*********** wst_check_w_% 文件转存为excel文件 ***********'''
    excel_out_pathf = "../wst_without_foreign_check/wst_check_w%.xlsx"
    save_to_excel(excel_out_pathf, wst_check_wf_01, wst_check_wf_10, add_column)





