import torch
import torch.nn as nn
import numpy as np
import cv2
import time
import matplotlib.pyplot as plt


a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
        [0,0,0,0,0, -7.21984,   4.77345, -17.08892, -19.98676,  -6.65397, -10.56651, -15.72788,  -4.23451, -11.30104],
        [0,0,0,0,0, 7.56419,  -5.15149,  -5.77484, -12.28424, -10.24066,  -8.07071,  -8.84589,  -9.55158, -11.28349],
        [0,0,0,0,0, -4.41505, -11.84778,   5.30759,  -4.94095,  -8.08970, -14.26806,  -8.22902, -14.45283, -11.11575],
        [0,0,0,0,0, -4.85881, -12.93312,   6.13393,  -4.75256,  -8.30600, -15.60899,  9.30687, -15.94003, -11.89202],
        [0,0,0,0,0, -9.16481,   4.85175, -18.92580, -18.22492, -10.74850,  -7.82335, -12.80189,  -2.70451, -12.57616],
        [0,1,0,0,0, -2.91972e+00, -2.20095e+00, 3.48893e+00, -2.59476e+00, -2.79146e+00, -2.14687e+00, -1.84426e+00, -2.32959e+00, -2.42477e+00],
        [0,0,1,0,0, 2.26935e+00, -2.67812e+00, -3.04553e+00, -3.46773e+00, -2.81742e+00, -2.19005e+00, -2.42943e+00, -2.88006e+00, -2.69119e+00],
        [0,0,0,1,0, -2.82522e+00, -2.65070e+00, -3.47379e+00, -1.19072e+00, -2.82531e+00, -2.53264e+00, -2.48552e+00, -3.06835e+00, -2.50541e+00],
        [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
        [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
        [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
        [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
        [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00],
        [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
        [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0., 1., 0., 0.]]

nc = 9

wst_check_w_01 = np.zeros(shape=(nc, nc), dtype=int)
wst_check_w_10 = np.zeros(shape=(nc, nc), dtype=int)
wst_check_w_01_ij = np.zeros(shape=(nc, nc), dtype=int)
wst_check_w_10_ij = np.zeros(shape=(nc, nc), dtype=int)
wst_check_0 = np.zeros(shape=(nc, nc), dtype=float)
wst_check_1 = np.zeros(shape=(nc, nc), dtype=float)

dy0_pre_right = 0
dy1_pre_right = 0
dy0_pre_wrong = 0
dy1_pre_wrong = 0



'''############################ 详细版 ############################'''
# ps = torch.tensor(a, requires_grad=True)
# t = torch.tensor(at)
# nb = ps.shape[0]
#
#
# '''====================== ps中的feature vecs做预处理 ======================'''
# _, k_t_id = torch.max(t, dim=1)      # find the class of every feature vec
# old_ps = ps.clone()
# new_ps = ps[:, 5:].clone()
# new_ps = torch.cat((k_t_id[:, None], new_ps), 1)      # feature vec里在第一个元素上加入类别标签
# print("\n k_t_id \n", k_t_id)
#
#
# '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
# '''ct：统计当前ps序列中所有出现的类别'''
# ct_idx = t.sum(dim=0)
# ct_idx = ct_idx.cpu().numpy()
# ct = np.argwhere(ct_idx != 0)
# ct = torch.from_numpy(ct)
# ct = ct.to(new_ps.device)
# print("\n ct \n", ct)
#
# '''
# idx_ci：当前ps序列中属于i类的行索引
# ps_ci：当前ps序列中属于i类的所以feature vecs的集合
# m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
# '''
# new_ps_n = new_ps.detach().cpu().numpy()      # 便于操作
#
# names = locals()      # 便于动态命名，使其变量名能与类别标签对应上
# for ci in range(9):
#     names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
#     if names['idx_c%s' % ci].size != 0:
#         names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
#         names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
#         names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
#         names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
#         '''画图，便于观察'''
#         # if names['ps_c%s' % ci].shape[0] != 0:
#         #         plt.figure()
#         #         x = np.arange(0, 9)
#         #         ps_ci_np = names['ps_c%s' % ci].detach().numpy()
#         #         m_ps_ci_np = names['m_ps_c%s' % ci].detach().numpy()
#         #         # print("ps_ci_np", ps_ci_np[0, :])
#         #         # print("x", x)
#         #         for i in range(ps_ci_np.shape[0]):
#         #             plt.plot(x, ps_ci_np[i, :], color='salmon', linestyle='--')
#         #         plt.plot(x, m_ps_ci_np, color='red', linewidth=3, label='means')
#         #         plt.xlabel("features_vec")
#         #         plt.ylabel("data")
#         #         plt.title("mean")
#         #         plt.show()
#
# '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
# for ci in range(ct.shape[0]):
#     temp_ci = int(ct[ci, 0])
#     if ci == 0:
#             m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
#     else:
#             m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
#
#
# '''====================== 计算差值 ======================'''
# '''
# rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
# idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
# '''
# for ci in range(ct.shape[0]):
#     temp_ci = int(ct[ci, 0])
#     names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
#
#     if ci == 0:
#             rd_cs_ps = names['rd_c%s' % temp_ci]
#             temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
#             idx_ct_ps = temp_idx
#     else:
#             rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
#             temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
#             idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)
#
# '''
# rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1）
# '''
# rd_all = torch.norm(old_ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
#
# '''
# rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
# rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
# '''
# idx_ct_ps = idx_ct_ps.to(torch.int64)
# rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
# rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)
# print('\n idx_ct_ps \n', idx_ct_ps)
# print('\n rt_all_ct_ps \n', rt_all_ct_ps)
#
# '''
# rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
# rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
# '''
# rd_cs_ps_d0 = rd_cs_ps[None, :].t()
# temp_0_min = torch.min(rd_cs_ps_d0)
# temp_0_max = torch.max(rd_cs_ps_d0)
# rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)
# print('\n rd_cs_ps_d1 \n', rd_cs_ps_d1)
#
# ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
# for ci in range(ct.shape[0]):
#     temp_ci = int(ct[ci, 0])
#     if ci == 0:
#             ci_idx = ci
#     else:
#             ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
#     names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#     names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#     names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
#     names['dy1_c%s_ct' % temp_ci] = ct[torch.arange(ct.size(0)) != ci]
#     # print("\n names['dy1_c%s_ct] = %s \n" % (temp_ci, names['dy1_c%s_ct' % temp_ci]))
#
#     if ci == 0:
#             temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
#             temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
#     else:
#             temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
#             temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min
#
#
# ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
# for ci in range(ct.shape[0]):
#     temp_ci = int(ct[ci, 0])
#     if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
#             names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
#             names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci], (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#             # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#             temp_1_mid = 0.5
#             names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)
#             names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])
#
#             names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] + 0.001
#             names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci], (0.002 + (temp_0_max + 0.001)))
#             # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
#             temp_0_mid = 0.5
#             names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - temp_0_mid)
#             names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])
#
#             names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
#             # print("\n ========================= \n dy0_pre_c%s_p = \n %s \n" % (temp_ci, names['dy0_pre_c%s_p' % temp_ci]))
#
#             wst_check_0[temp_ci, temp_ci] = names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]
#             # print("\n wst_check_0 \n", wst_check_0)
#
#             dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
#             dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()
#             # print("\n dy0_pre_right \n", dy0_pre_right)
#             # print("\n dy0_pre_wrong \n", dy0_pre_wrong)
#
#             names['dy1_pre_c%s_p' % temp_ci] = (torch.tanh(
#                 (names['rd_cs_ps_d1_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
#             # print("\n  dy1_pre_c%s_p = \n %s \n" % (temp_ci, names['dy1_pre_c%s_p' % temp_ci]))
#
#             dy1_pre_right += names['dy1_pre_c%s_p' % temp_ci].sum()
#             dy1_pre_wrong += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0] * names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[1] - names['dy1_pre_c%s_p' % temp_ci].sum()
#             # print("\n dy1_pre_right \n", dy1_pre_right)
#             # print("\n dy1_pre_wrong \n", dy1_pre_wrong)
#
#             for cj in range(names['dy1_c%s_ct' % temp_ci].shape[1]):
#                 temp_cj = int(names['dy1_c%s_ct' % temp_ci][cj, 0])
#                 wst_check_1[temp_ci, temp_cj] = names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0]
#                 # print("\n wst_check_1 \n", wst_check_1)
#
#                 for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
#                     if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
#                         wst_check_w_01[temp_ci, temp_ci] += 1
#
#                         if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
#                             wst_check_w_01_ij[temp_ci, temp_cj] += 1
#
#                     if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
#                         wst_check_w_10[temp_ci, temp_cj] += 1
#
#                         if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 0.0:
#                             wst_check_w_10_ij[temp_cj, temp_ci] += 1
#             # print("\n wst_check_w_01 \n", wst_check_w_01)
#             # print("\n wst_check_w_10 \n", wst_check_w_10)
#
#
#
# # for ci in range(ct.shape[0]):
# #     temp_ci = int(ct[ci, 0])
# #     names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
# #     # print("\n ========================= \n"
# #     #       "dy0_pre_c%s_p = \n %s \n" % (temp_ci, names['dy0_pre_c%s_p' % temp_ci]))
# #
# #     wst_check_0[temp_ci, temp_ci] = names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]
# #     # print("\n wst_check_0 \n", wst_check_0)
# #
# #     dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
# #     dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()
# #     # print("\n dy0_pre_right \n", dy0_pre_right)
# #     # print("\n dy0_pre_wrong \n", dy0_pre_wrong)
# #
# #     names['dy1_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d1_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
# #     # print("\n  dy1_pre_c%s_p = \n %s \n" % (temp_ci, names['dy1_pre_c%s_p' % temp_ci]))
# #
# #     dy1_pre_right += names['dy1_pre_c%s_p' % temp_ci].sum()
# #     dy1_pre_wrong += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0] * names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[1] - \
# #                      names['dy1_pre_c%s_p' % temp_ci].sum()
# #     # print("\n dy1_pre_right \n", dy1_pre_right)
# #     # print("\n dy1_pre_wrong \n", dy1_pre_wrong)
# #
# #     for cj in range(names['dy1_c%s_ct' % temp_ci].shape[1]):
# #         temp_cj = int(names['dy1_c%s_ct' % temp_ci][cj, 0])
# #         wst_check_1[temp_ci, temp_cj] = names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0]
# #         # print("\n wst_check_1 \n", wst_check_1)
# #
# #         for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
# #             if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
# #                 wst_check_w_01[temp_ci, temp_ci] += 1
# #
# #                 if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
# #                     wst_check_w_01_ij[temp_ci, temp_cj] += 1
# #
# #             if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
# #                 wst_check_w_10[temp_ci, temp_cj] += 1
# #
# #                 if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 0.0:
# #                     wst_check_w_10_ij[temp_cj, temp_ci] += 1
# #     # print("\n wst_check_w_01 \n", wst_check_w_01)
# #     # print("\n wst_check_w_10 \n", wst_check_w_10)
#
# print("\n wst_check_0 \n", wst_check_0)
# print("\n wst_check_1 \n", wst_check_1)
#
# print("\n dy0_pre_right \n", dy0_pre_right)
# print("\n dy0_pre_wrong \n", dy0_pre_wrong)
# print("\n dy1_pre_right \n", dy1_pre_right)
# print("\n dy1_pre_wrong \n", dy1_pre_wrong)
# print("\n wst_check_w_01 \n", wst_check_w_01)
# print("\n wst_check_w_01_ij \n", wst_check_w_01_ij)
# print("\n wst_check_w_10 \n", wst_check_w_10)
# print("\n wst_check_w_10_ij \n", wst_check_w_10_ij)




'''############################ 精简版 ############################'''
ps = torch.tensor(a, requires_grad=True)
t = torch.tensor(at)
nb = ps.shape[0]
nc = 9

'''====================== ps中的feature vecs做预处理 ======================'''
_, k_t_id = torch.max(t, dim=1)      # find the class of every feature vec
old_ps = ps.detach()
new_ps = ps[:, 5:].detach()
new_ps = torch.cat((k_t_id[:, None], new_ps), 1)      # feature vec里在第一个元素上加入类别标签

wst_check_w_01 = torch.from_numpy(wst_check_w_01).to(new_ps.device)
wst_check_w_10 = torch.from_numpy(wst_check_w_10).to(new_ps.device)
wst_check_w_01_ij = torch.from_numpy(wst_check_w_01_ij).to(new_ps.device)
wst_check_w_10_ij = torch.from_numpy(wst_check_w_10_ij).to(new_ps.device)
wst_check_0 = torch.from_numpy(wst_check_0).to(new_ps.device)
wst_check_1 = torch.from_numpy(wst_check_1).to(new_ps.device)

'''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
'''
ct：统计当前ps序列中所有出现的类别
'''
ct_idx = t.sum(dim=0)
ct_idx = ct_idx.cpu().numpy()
ct = np.argwhere(ct_idx != 0)
ct = torch.from_numpy(ct)
ct = ct.to(new_ps.device)
# print("\n ct \n", ct)

'''
idx_ci：当前ps序列中属于i类的行索引
ps_ci：当前ps序列中属于i类的所以feature vecs的集合
m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
'''
new_ps_n = new_ps.detach().cpu().numpy()      # 便于操作

names = locals()      # 便于动态命名，使其变量名能与类别标签对应上
for ci in range(nc):
    names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
    if names['idx_c%s' % ci].size != 0:
        names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
        names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
        names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
        names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
        '''画图，便于观察'''
        # if names['ps_c%s' % ci].shape[0] != 0:
        #         plt.figure()
        #         x = np.arange(0, nc)
        #         ps_ci_np = names['ps_c%s' % ci].detach().numpy()
        #         m_ps_ci_np = names['m_ps_c%s' % ci].detach().numpy()
        #         # print("ps_ci_np", ps_ci_np[0, :])
        #         # print("x", x)
        #         for i in range(ps_ci_np.shape[0]):
        #             plt.plot(x, ps_ci_np[i, :], color='salmon', linestyle='--')
        #         plt.plot(x, m_ps_ci_np, color='red', linewidth=3, label='means')
        #         plt.xlabel("features_vec")
        #         plt.ylabel("data")
        #         plt.title("mean")
        #         plt.show()

'''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
for ci in range(ct.shape[0]):
    temp_ci = int(ct[ci, 0])
    if ci == 0:
            m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
            m_ps_cs_wcl = m_ps_ci_wcl
    else:
            m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
            m_ps_ci_wcl = names['m_ps_c%s' % temp_ci].unsqueeze(0)
            m_ps_ci_wcl = torch.cat((ct[ci].unsqueeze(0), m_ps_ci_wcl), 1)
            m_ps_cs_wcl = torch.cat((m_ps_cs_wcl, m_ps_ci_wcl), 0)
# print('\n m_ps_cs_wcl \n', m_ps_cs_wcl)
# '''画图，便于观察'''
# plt.figure()
# x = np.arange(0, nc)
# m_ps_cs_wcl_np = m_ps_cs_wcl.detach().cpu().numpy()
# for i in range(m_ps_cs_wcl_np.shape[0]):
#     class_nam = str(m_ps_cs_wcl_np[i, 0])
#     plt.plot(x, m_ps_cs_wcl_np[i, 1:], linestyle='--', label='class_nam')
# plt.xlabel("features_vec")
# plt.ylabel("data")
# plt.title("mean")
# plt.show()


'''====================== 计算差值 ======================'''
'''
rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
'''
for ci in range(ct.shape[0]):
    temp_ci = int(ct[ci, 0])
    names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
    if ci == 0:
            rd_cs_ps = names['rd_c%s' % temp_ci]
            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
            idx_ct_ps = temp_idx
    else:
            rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
            temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
            idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

'''
rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1）
'''
rd_all = torch.norm(old_ps[:, None, 5:] - m_ps_cs, dim=2, p=1)

'''
rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
'''
idx_ct_ps = idx_ct_ps.to(torch.int64)
rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)

'''
rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
'''
rd_cs_ps_d0 = rd_cs_ps[None, :].t()
temp_0_min = torch.min(rd_cs_ps_d0)
temp_0_max = torch.max(rd_cs_ps_d0)
rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)

''' 
rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 
'''
for ci in range(ct.shape[0]):
    temp_ci = int(ct[ci, 0])
    if ci == 0:
            ci_idx = ci
    else:
            ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
    names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
    names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
    names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
    names['dy1_c%s_ct' % temp_ci] = ct[torch.arange(ct.size(0)) != ci]
    # print("\n names['dy1_c%s_ct] = %s \n" % (temp_ci, names['dy1_c%s_ct' % temp_ci]))
    if ci == 0:
            temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
            temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
    else:
            temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
            temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min

''' 
rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid 
'''
for ci in range(ct.shape[0]):
    temp_ci = int(ct[ci, 0])
    if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
            names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
            names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci], (0.002 + (temp_1_max + 0.001 - temp_1_min)))
            # temp_1_mid = torch.div(10, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
            temp_1_mid = 0.5
            names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)
            names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])

            names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] + 0.001
            names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci], (0.002 + (temp_0_max + 0.001)))
            # temp_0_mid = torch.div(5, (0.002 + (temp_0_max + 0.001)))
            temp_0_mid = 0.5
            names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - temp_0_mid)
            names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])

            names['dy0_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2
            wst_check_0[temp_ci, temp_ci] = names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0]

            dy0_pre_wrong += names['dy0_pre_c%s_p' % temp_ci].sum()
            dy0_pre_right += names['rd_cs_ps_d0_c%s_p' % temp_ci].shape[0] - names['dy0_pre_c%s_p' % temp_ci].sum()

            names['dy1_pre_c%s_p' % temp_ci] = (torch.tanh((names['rd_cs_ps_d1_c%s_p' % temp_ci] - 0.5) * 10000) + 1) / 2

            dy1_pre_right += names['dy1_pre_c%s_p' % temp_ci].sum()
            dy1_pre_wrong += names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0] * names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[1] - names['dy1_pre_c%s_p' % temp_ci].sum()

            for cj in range(names['dy1_c%s_ct' % temp_ci].shape[1]):
                temp_cj = int(names['dy1_c%s_ct' % temp_ci][cj, 0])
                wst_check_1[temp_ci, temp_cj] = names['rd_cs_ps_d1_c%s_p' % temp_ci].shape[0]

                for i in range(names['dy0_pre_c%s_p' % temp_ci].shape[0]):
                    if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 1.0:
                        wst_check_w_01[temp_ci, temp_ci] += 1

                        if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                            wst_check_w_01_ij[temp_ci, temp_cj] += 1

                    if names['dy1_pre_c%s_p' % temp_ci][i, cj] == 0.0:
                        wst_check_w_10[temp_ci, temp_cj] += 1

                        if names['dy0_pre_c%s_p' % temp_ci][i, 0] == 0.0:
                            wst_check_w_10_ij[temp_cj, temp_ci] += 1

m_ps_cs_wcl = m_ps_cs_wcl.cpu().numpy()

wst_check_w_01 = wst_check_w_01.cpu().numpy()
wst_check_w_10 = wst_check_w_10.cpu().numpy()
wst_check_w_01_ij = wst_check_w_01_ij.cpu().numpy()
wst_check_w_10_ij = wst_check_w_10_ij.cpu().numpy()
wst_check_0 = wst_check_0.cpu().numpy()
wst_check_1 = wst_check_1.cpu().numpy()

dy0_pre_right = dy0_pre_right.cpu().numpy()
dy1_pre_right = dy1_pre_right.cpu().numpy()
dy0_pre_wrong = dy0_pre_wrong.cpu().numpy()
dy1_pre_wrong = dy1_pre_wrong.cpu().numpy()

print("\n m_ps_cs_wcl \n", m_ps_cs_wcl)
print("\n wst_check_0 \n", wst_check_0)
print("\n wst_check_1 \n", wst_check_1)
print("\n dy0_pre_right \n", dy0_pre_right)
print("\n dy0_pre_wrong \n", dy0_pre_wrong)
print("\n dy1_pre_right \n", dy1_pre_right)
print("\n dy1_pre_wrong \n", dy1_pre_wrong)
print("\n wst_check_w_01 \n", wst_check_w_01)
print("\n wst_check_w_01_ij \n", wst_check_w_01_ij)
print("\n wst_check_w_10 \n", wst_check_w_10)
print("\n wst_check_w_10_ij \n", wst_check_w_10_ij)









# wst_d = torch.zeros([nb, nb])
# wst_d_ps = ps.clone()
# wst_d_ps = wst_d_ps.cpu()
#
# wst_t = torch.zeros([nb, nb])
# wst_d_t = t.detach()
# wst_d_t = wst_d_t.cpu()
#
#
# for wst_di in range(nb):
#     for wst_dj in range(wst_di + 1, nb):
#         # wst_d[wst_di, wst_dj] = torch.sub(wst_d_ps[wst_di, 5:], wst_d_ps[wst_dj, 5:]).abs().sum()
#         # wst_d[wst_dj, wst_di] = wst_d[wst_di, wst_dj]
#         wst_t[wst_di, wst_dj] = -1.0 if torch.equal(wst_d_t[wst_di], wst_d_t[wst_dj]) else 1.0
#         wst_t[wst_dj, wst_di] = wst_t[wst_di, wst_dj]
#         '''********** wst的评价模块->check wst-w **********'''  # 4
#         if torch.equal(wst_d_t[wst_di], wst_d_t[wst_dj]):
#             wst_check_0[k_t_id[wst_di], k_t_id[wst_dj]] += 1
#         else:
#             wst_check_1[k_t_id[wst_di], k_t_id[wst_dj]] += 1
#
# print("\n wst_check_0 \n", wst_check_0)
# print("\n wst_check_1 \n", wst_check_1)
#
# '''==================================== 加速计算，不用for循环 ===================================='''
# # wst_d = torch.zeros([nb, nb])
# wst_d = torch.norm(wst_d_ps[:, None, 5:] - wst_d_ps[:, 5:], dim=2, p=1)
# print("\n wst_d \n", wst_d)
#
# wst_pre = wst_d.to(ps.device)
# wst_target = wst_t.to(t.device)
#
# pred_min = torch.min(wst_pre)
# pred_prob = wst_pre + 0.001
# pred_max = torch.max(pred_prob)
# '''************** 无sigmoid **************'''  # 1
# # pred_prob = pred_prob / (pred_max + 0.002)
# '''************** 有sigmoid **************'''  # 3
# pred_prob = pred_prob / (pred_max + 0.002)
# pred_prob = 10 * (pred_prob - 0.5)
# pred_prob = torch.sigmoid(pred_prob)
#
# for di in range(nb):
#     for dj in range(di + 1, nb):
#         if wst_target[di, dj] == -1.0:
#             if pred_prob[di, dj] < 0.5:
#                 dy0_pre_right += 1
#             else:
#                 dy0_pre_wrong += 1
#                 '''********** wst的评价模块->check wst-w **********'''  # 1
#                 wst_check_w_01[k_t_id[di], k_t_id[dj]] += 1
#
#         if wst_target[di, dj] == 1.0:
#             if pred_prob[di, dj] >= 0.5:
#                 dy1_pre_right += 1
#             else:
#                 dy1_pre_wrong += 1
#                 '''********** wst的评价模块->check wst-w **********'''  # 1
#                 wst_check_w_10[k_t_id[di], k_t_id[dj]] += 1
#
# print("\n dy0_pre_right \n", dy0_pre_right)
# print("\n dy0_pre_wrong \n", dy0_pre_wrong)
# print("\n dy1_pre_right \n", dy1_pre_right)
# print("\n dy1_pre_wrong \n", dy1_pre_wrong)
# print("\n wst_check_w_01 \n", wst_check_w_01)
# print("\n wst_check_w_10 \n", wst_check_w_10)