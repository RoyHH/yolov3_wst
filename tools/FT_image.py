import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import glob
from PIL import Image
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

def mkdir(path):
    # 判断路径是否存在，若不存在则创建
    isExists = os.path.exists(path)
    if not isExists:
        # 创建目录操作函数
        os.makedirs(path)
#cv图片展示通用代码
def cv_show(name,img):
    cv2.imshow(name, img)
    cv2.waitKey(0)  # 等待时间毫秒级，2000ms,0表示任意键终止
    cv2.destroyAllWindows()
#cv颜色通道转成plt颜色通道
def cv_plt(img):
    b, g, r = cv2.split(img)
    img_plt = cv2.merge([r, g, b])
    return img_plt

def FT(img1, Outpath, image_pre, ext):
    img_gray_float32 = np.float32(img1)

    #傅里叶变换
    dft = cv2.dft(img_gray_float32, flags=cv2.DFT_COMPLEX_OUTPUT)
    #将低频的部分拉到中间位置
    dft_shift = np.fft.fftshift(dft)

    #傅里叶变换后的数据需要展示，所以将其转换到图片形式，即将实部和虚部映射回图像格式数据，得到灰度图
    magnitude_spectum = 20*np.log(cv2.magnitude(dft_shift[:, :, 0], dft_shift[:, :, 1]))

    # plt.subplot(121), plt.imshow(img_gray, cmap='gray')
    # plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(122), plt.imshow(magnitude_spectum, cmap='gray')
    # plt.title('Magnitude Spectum'), plt.xticks([]), plt.yticks([])
    # plt.show()

    newFileName = image_pre + "_FT" + ext
    print("FT： ", newFileName)

    #图像归一化 并 保存
    result = np.zeros(magnitude_spectum.shape, dtype=np.float32)
    cv2.normalize(magnitude_spectum, result, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    cv2.imwrite(Outpath + newFileName, result*255.0)

def FT_H(img1, Outpath, image_pre, ext):
    img_gray_float32 = np.float32(img1)

    dft = cv2.dft(img_gray_float32, flags=cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)

    #中心位置
    rows, cols = img1.shape
    crow, ccol = int(rows/2), int(cols/2)

    #高频滤波
    mask = np.ones((rows, cols, 2), np.uint8)
    mask[crow-10:crow+10, ccol-10:ccol+10] = 0

    #IDFT
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])

    # plt.subplot(121), plt.imshow(img1, cmap='gray'), plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(122), plt.imshow(img_back, cmap='gray'), plt.title('Result'), plt.xticks([]), plt.yticks([])
    # plt.imshow(img_back)
    # plt.show()
    newFileName = image_pre + "_FT_H" + ext
    print("FT_H： ", newFileName)

    #图像归一化 并 保存
    result = np.zeros(img_back.shape, dtype=np.float32)
    cv2.normalize(img_back, result, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    cv2.imwrite(Outpath + newFileName, result*255.0)

def FT_L(img1, Outpath, image_pre, ext):
    img_gray_float32 = np.float32(img1)

    dft = cv2.dft(img_gray_float32, flags=cv2.DFT_COMPLEX_OUTPUT)
    dft_shift = np.fft.fftshift(dft)

    #锚定中心位置
    rows, cols = img1.shape
    crow, ccol = int(rows/2), int(cols/2)

    #低频滤波：在中心点框出60*60的矩形，并赋值为1，作为滤频区域
    mask = np.zeros((rows, cols, 2), np.uint8)
    mask[crow-50:crow+50, ccol-50:ccol+50] = 1

    #IDFT：傅里叶逆变换
    fshift = dft_shift*mask
    f_ishift = np.fft.ifftshift(fshift)
    img_back = cv2.idft(f_ishift)
    img_back = cv2.magnitude(img_back[:, :, 0], img_back[:, :, 1])

    # plt.subplot(121), plt.imshow(img_gray, cmap='gray'), plt.title('Input Image'), plt.xticks([]), plt.yticks([])
    # plt.subplot(122), plt.imshow(img_back, cmap='gray'), plt.title('Result'), plt.xticks([]), plt.yticks([])
    # plt.show()

    newFileName = image_pre + "_FT_L" + ext
    print("FT_L： ", newFileName)

    #图像归一化 并 保存
    result = np.zeros(img_back.shape, dtype=np.float32)
    cv2.normalize(img_back, result, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    cv2.imwrite(Outpath + newFileName, result*255.0)

def Dif_image(img1, image_pre, ext, InFile_normal_path):
    for Parent, Dir_Name, File_Name in os.walk(InFile_normal_path):
        Imgpath = InFile_normal_path + '\\'
        for fileName in os.listdir(Imgpath):
            image_pre_normal, ext_normal = os.path.splitext(fileName)
            img1_normal = cv2.imread(Imgpath + fileName, cv2.IMREAD_GRAYSCALE)
            if image_pre_normal == image_pre:
                img1 = img1 - img1_normal
                return img1


if __name__ == '__main__':
    InFile_normal_path = "../data/sample1_normal"
    InFile_path = "../data/sample1"
    OutFile_path = "../data/FT_sample"
    mkdir(OutFile_path)

    imgae_re = glob.glob('../data/FT_sample/*')
    for i in imgae_re: os.remove(i)

    for Parent, Dir_Name, File_Name in os.walk(InFile_path):
        Imgpath = InFile_path + '\\'
        Outpath = OutFile_path + '\\'
        for fileName in os.listdir(Imgpath):
            image_pre, ext = os.path.splitext(fileName)

            img = cv2.imread(Imgpath + fileName, cv2.IMREAD_GRAYSCALE)
            img1 = img.copy()

            # img11 = Dif_image(img1, image_pre, ext, InFile_normal_path)
            # plt.subplot(121), plt.imshow(img1, cmap='gray'), plt.title('Input Image'), plt.xticks([]), plt.yticks([])
            # plt.subplot(122), plt.imshow(img11, cmap='gray'), plt.title('Result'), plt.xticks([]), plt.yticks([])
            # plt.show()

            '''========================傅里叶变换==========================
                傅里叶变换的物理意义————把图片转换成由中心低频向外逐渐放射高频的频率图
                    •高频 变化剧烈 边界区域
                    •低频 变化缓慢 图像内部
                dft：傅里叶变换
                idft：傅里叶逆变换
                傅里叶变换作用————滤波：
                    •低频滤波器 保留低频，使图像模糊
                    •高频滤波器 保留高频，使图像细节增强
                    •opencv中主要是cv2.dft()和cv2.idft(),输入图像先转换成np.float32格式。
                    •通常频率为0在左上方，shift为转换到中间
                    •cv2.dft()返回结果为双通道（实部，虚部）通常需要转换格式（0,255）
            '''
            FT(img1, Outpath, image_pre, ext)

            '''========================傅里叶变换————高频滤波==========================
            高频滤波器 保留高频，滤掉低频，也就是说图像中心区域会被滤掉，而边界处的图像会变得清晰，类似描边
            '''
            FT_H(img1, Outpath, image_pre, ext)

            '''========================傅里叶变换————低频滤波==========================
            低频滤波器 保留低频，滤掉高频，也就是说图像中心区域会保持不变，而边界处的图像会变得模糊
            '''
            FT_L(img1, Outpath, image_pre, ext)
