import torch
import torch.nn as nn
import numpy as np
import cv2
import time
import matplotlib.pyplot as plt

a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
        [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
        [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
        [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
        [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
        [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00]]
        # [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
        # [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.]]
        # [0., 0., 0., 0., 0., 1., 0., 0., 0.],
        # [0., 0., 0., 0., 0., 0., 0., 1., 0.]]


def batch_norm_v1(x, gamma, beta, moving_means, moving_vars, moving_momentum, is_train=True, bn_dim=0):
    eps = 1e-5
    x_mean = torch.mean(x, dim=bn_dim)
    x_var = torch.sqrt(torch.mean((x - x_mean) ** 2, dim=bn_dim))
    if is_train == True:
        moving_means = moving_momentum * moving_means + (1. - moving_momentum) * x_mean
        moving_vars = moving_momentum * moving_vars + (1. - moving_momentum) * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means.detach(), moving_vars.detach()
    else:
        x_mean = (1. - moving_momentum) * moving_means + moving_momentum * x_mean
        x_var = (1. - moving_momentum) * moving_vars + moving_momentum * x_var
        x_hat = (x - x_mean) / (x_var + eps)
        return gamma * x_hat + beta, moving_means, moving_vars


def check_wrong(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                wst_pre, wst_d_ps, pred, temp_c, key='Nan'):
    print('\n \n =================== Have %s =================== ' % (key))
    print(' Have nan: \n loss[%s] = \n%s ' % (ss, loss[ss]))
    print(' Similar:        loss_y0[%s] =\n %s ' % (ss, loss_y0[ss]))
    print(' Different:      loss_y1[%s] = \n%s ' % (ss, loss_y1[ss]))
    print(' Label:          wst_target[%s] = \n%s ' % (ss, wst_target[ss]))
    print(' pred-BN-dis-sg: pred_prob[%s] = \n%s ' % (ss, pred_prob[ss]))
    print(' pred-BN-dis:    wst_pre[%s] = \n%s ' % (ss, wst_pre[ss]))

    for temp_i in range(nb):
        temp_s = ((nb - 1) + (nb - 1 - (temp_i - 1))) * temp_i * 0.5
        # print("\n temp_s == ", temp_s)
        if temp_s > ss:
            temp_ss = ((nb - 1) + (nb - 1 - (temp_i - 2))) * (temp_i - 1) * 0.5
            # print("\n temp_ss == ", temp_ss)
            temp_j = int(ss - temp_ss + temp_i)
            # print("\n temp_j == ", temp_j)
            print("\n ************************ N0. %s************************************ " % (temp_c))
            print(' ----------- After BN -----------')
            print(' Have %s: \n ni-wst_d_ps[%s] = %s ' % (key, temp_i - 1, wst_d_ps[temp_i - 1, 5:]))
            print(' nj-wst_d_ps[%s] = %s ' % (temp_j, wst_d_ps[temp_j, 5:]))
            print(' ----------- Before BN -----------')
            print(' Have %s: \n ni-pred[%s] = %s ' % (key, temp_i - 1, pred[temp_i - 1, 5:]))
            print(' nj-pred[%s] = %s ' % (temp_j, pred[temp_j, 5:]))
            break

def check_wrong_cos(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                    wst_pre, wst_d, wst_temp, wst_l2_temp, wst_d_temp,
                    wst_d_ps, pred, temp_c, key='Nan'):
    print('\n \n =================== Have %s =================== ' % (key))
    print(' Have nan:        loss[%s] = %s ' % (ss, loss[ss]))
    print(' Similar:         loss_y0[%s] = %s ' % (ss, loss_y0[ss]))
    print(' Different:       loss_y1[%s] = %s ' % (ss, loss_y1[ss]))
    print(' Label:           wst_target[%s] = %s ' % (ss, wst_target[ss]))
    print(' pred-BN-dis-sg:  pred_prob[%s] = %s ' % (ss, pred_prob[ss]))
    print(' pred-BN-(1-cos): wst_pre[%s] = %s ' % (ss, wst_pre[ss]))
    print(' pred-BN-cos:     wst_d[%s] = %s ' % (ss, wst_d[ss]))

    for temp_i in range(nb):
        temp_s = ((nb - 1) + (nb - 1 - (temp_i - 1))) * temp_i * 0.5
        # print("\n temp_s == ", temp_s)
        if temp_s > ss:
            temp_ss = ((nb - 1) + (nb - 1 - (temp_i - 2))) * (temp_i - 1) * 0.5
            # print("\n temp_ss == ", temp_ss)
            temp_j = int(ss - temp_ss + temp_i)
            # print("\n temp_j == ", temp_j)
            print("\n ************************ N0. %s************************************ " % (temp_c))
            print(' ----------- Cosine(a,b) -----------')
            print(' Have %s: \n nij-wst_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                               wst_temp[temp_i - 1, temp_j]))
            print(' ----------- |a|*|b| -----------')
            print(' Have %s: \n nij-wst_l2_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                                  wst_l2_temp[temp_i - 1, temp_j]))
            print(' ----------- Sum（a*b） -----------')
            print(' Have %s: \n nij-wst_d_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                                                                 wst_d_temp[temp_i - 1, temp_j]))
            print(' ----------- After BN -----------')
            print(' Have %s: \n ni-wst_d_ps[%s] = %s ' % (key, temp_i - 1, wst_d_ps[temp_i - 1, 5:]))
            print(' nj-wst_d_ps[%s] = %s ' % (temp_j, wst_d_ps[temp_j, 5:]))
            print(' ----------- Before BN -----------')
            print(' Have %s: \n ni-pred[%s] = %s ' % (key, temp_i - 1, pred[temp_i - 1, 5:]))
            print(' nj-pred[%s] = %s ' % (temp_j, pred[temp_j, 5:]))
            break


pred = torch.tensor(a)
true = torch.tensor(at)

nb = pred.shape[0]
wst_d_ps = pred.clone()
wst_d_t = true.detach()

measure_loss = 'Cosine'

epoch = 18
epochs = 300
wst_nc = 9

is_train = True

fe_means = torch.zeros((epochs, wst_nc))
fe_vars = torch.zeros((epochs, wst_nc))
fe_move = torch.tensor(0.1)

'''============ 特征向量预处理 BN操作 ============'''
gamma_bn = torch.ones(1)
beta_bn = torch.zeros(1)
# 各特征维度做BN，在单一特征维度上保持正态分布
m_m = torch.tensor((10.0 * epoch / epochs))
m_mfe = torch.sigmoid(m_m) * fe_move
wst_d_ps[:, 5:], fe_means[epoch, :], fe_vars[epoch, :] = \
    batch_norm_v1(wst_d_ps[:, 5:], gamma_bn, beta_bn, fe_means[epoch, :],
                       fe_vars[epoch, :], m_mfe, is_train=is_train)

# print('\n m_mfe = %s' % (m_mfe))
# print('\n wst_d_ps[:, 5:] = %s' % (wst_d_ps[:, 5:]))

''' 关键点位加权 '''
# wst_d_ps = self.key_add_weight(wst_d_ps, add_weight=10.0)

if measure_loss == 'L1':
    '''～～～～～～～～～～～～～～～～～ L1 版本 ～～～～～～～～～～～～～～～～～'''
    wst_pre = torch.pdist(wst_d_ps[:, 5:], p=1)
    wst_target = torch.pdist(wst_d_t, p=1) / 2
    # print('\n wst_target = %s' % (wst_target))

    pred_min = torch.min(wst_pre)
    pred_prob = wst_pre - pred_min + 0.001
    pred_max = torch.max(pred_prob)
    pred_prob = pred_prob / (pred_max + 0.002)  # (0, 1)
    # print('\n pred_prob.(0,1) = %s' % (pred_prob))

    pred_prob = 12.0 * (pred_prob - 0.5)  # (-6, 6)
    # print('\n pred_prob.bnl = %s' % (pred_prob))

    pred_prob = torch.sigmoid(pred_prob)
    # print('\n pred_prob.sigmoid = %s' % (pred_prob))


elif measure_loss == 'L2':
    '''～～～～～～～～～～～～～～～～～ L2 版本 ～～～～～～～～～～～～～～～～～'''
    wst_pre = torch.pdist(wst_d_ps[:, 5:], p=2)
    wst_target = torch.pdist(wst_d_t, p=1) / 2
    pred_min = torch.min(wst_pre)
    pred_prob = wst_pre - pred_min + 0.001
    pred_max = torch.max(pred_prob)
    pred_prob = pred_prob / (pred_max + 0.002)  # (0, 1)
    pred_prob = 12.0 * (pred_prob - 0.5)  # (-6, 6)
    pred_prob = torch.sigmoid(pred_prob)
elif measure_loss == 'Cosine':
    '''～～～～～～～～～～～～～～～～～ Cosine 版本 ～～～～～～～～～～～～～～～～～'''
    wst_d_a = wst_d_ps[:, 5:].unsqueeze(dim=1)
    wst_d_b = wst_d_ps[:, 5:].unsqueeze(dim=0)
    wst_d_temp = wst_d_a * wst_d_b
    wst_d_temp = wst_d_temp.sum(dim=2)
    wst_l2 = torch.norm(wst_d_ps[:, 5:], dim=1, p=2)
    wst_l2_a = wst_l2.unsqueeze(dim=1)
    wst_l2_b = wst_l2.unsqueeze(dim=0)
    wst_l2_temp = wst_l2_a * wst_l2_b
    wst_temp = wst_d_temp / wst_l2_temp  # (-1, 1)
    print('\n cosine---wst_temp = %s' % (wst_temp))

    # print('wst_temp', wst_temp)
    for i in range(wst_temp.shape[0]):
        if i == 0:
            wst_d = wst_temp[0, i + 1:]
        else:
            wst_d = torch.cat((wst_d, wst_temp[i, i + 1:]), 0)
    # print('wst_d', wst_d)
    wst_target = torch.pdist(wst_d_t, p=1) / 2
    print('\n cosine---wst_d = %s' % (wst_d))

    wst_d = torch.tensor([-1, 0.9921, -1, 0.9865, 0.9937, 0.0063, 0.0028, 0.0091, 0.0031,
        0.0111, 0.0025, 0.0119, 0.0185, 0.0052, 1])
    wst_pre = 1.0 - wst_d  # (0, 2)
    print('\n cosine---wst_pre = %s' % (wst_pre))

    pred_prob = 6.0 * (wst_pre - 1.0)  # (-6, 6)
    # pred_prob = torch.tensor([6, 0.9921, 6, 0.9865, 0.9937, 0.0063, 0.0028, 0.0091, 0.0031,
    #     0.0111, 0.0025, 0.0119, 0.0185, 0.0052, -6.0])
    pred_prob = torch.sigmoid(pred_prob)
    print('\n bnl---sigmoid---pred_prob = %s' % (pred_prob))

    pred_prob = torch.tensor([1, 0.9921, 1, 0.9865, 0.9937, 0.0063, 0.0028, 0.0091, 0.0031,
        0.0111, 0.0025, 0.0119, 0.0185, 0.0052, 0.0])
    print('\n pred_prob = %s' % (pred_prob))

''' BCELoss Loss '''
loss_y1 = - wst_target * torch.log(pred_prob)
loss_y0 = - (1 - wst_target) * torch.log(1 - pred_prob)
''' Focal Loss '''
# loss_y1 = - (1 - pred_prob) ** self.gamma * wst_target * self.alpha * torch.log(pred_prob)
# loss_y0 = - (pred_prob) ** self.gamma * (1 - wst_target) * (1 - self.alpha) * torch.log(1 - pred_prob)
# loss_y1 = - (1 - pred_prob) ** self.gamma * wst_target * self.alpha * torch.log(pred_prob)
# loss_y0 = - (pred_prob) ** self.gamma * (1 - wst_target) * (1 - self.alpha) * torch.log(1 - pred_prob) - \
#           (1 - pred_prob) ** self.gamma * (1 - wst_target) * self.omg * torch.log(pred_prob)
loss = loss_y0 + loss_y1

nl = loss.shape[0]
ii = 0

if torch.isnan(loss.sum()):
    print('\n loss_sum = %s ' % (loss.sum()))
    print('\n loss_sum_y0 = %s ' % (loss_y0.sum()))
    print('\n loss_sum_y1 = %s ' % (loss_y1.sum()))
    if measure_loss == 'L1' or measure_loss == 'L2':
        print("\n pred_min + 0.001 == ", pred_min + 0.001)
        print("\n pred_max == ", pred_max)

        temp_nan_c = 0
        temp_inf_c = 0
        for ss in range(wst_pre.shape[0]):
            check_wst_pre = loss.detach().cpu().numpy()

            if np.isnan(check_wst_pre[ss]):
                temp_nan_c = temp_nan_c + 1
                check_wrong(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                                 wst_pre, wst_d_ps, pred, temp_nan_c, key='Nan')

            if check_wst_pre[ss] == np.inf:
                temp_inf_c = temp_inf_c + 1
                check_wrong(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                                 wst_pre, wst_d_ps, pred, temp_inf_c, key='Inf')

    elif measure_loss == 'Cosine':
        temp_nan_c = 0
        temp_inf_c = 0

        for ss in range(wst_pre.shape[0]):
            check_wst_pre = loss.detach().cpu().numpy()

            if np.isnan(check_wst_pre[ss]):
                temp_nan_c = temp_nan_c + 1
                check_wrong_cos(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                                wst_pre, wst_d, wst_temp, wst_l2_temp, wst_d_temp,
                                wst_d_ps, pred, temp_nan_c, key='Nan')

            if check_wst_pre[ss] == np.inf:
                temp_inf_c = temp_inf_c + 1
                check_wrong_cos(nb, ss, loss, loss_y0, loss_y1, wst_target, pred_prob,
                                wst_pre, wst_d, wst_temp, wst_l2_temp, wst_d_temp,
                                wst_d_ps, pred, temp_inf_c, key='Inf')

                # temp_c = temp_nan_c
                # key = 'Nan'
                # print('\n \n =================== Have %s =================== ' % (key))
                # print(' Have nan:        loss[%s] = %s ' % (ss, loss[ss]))
                # print(' Similar:         loss_y0[%s] = %s ' % (ss, loss_y0[ss]))
                # print(' Different:       loss_y1[%s] = %s ' % (ss, loss_y1[ss]))
                # print(' Label:           wst_target[%s] = %s ' % (ss, wst_target[ss]))
                # print(' pred-BN-dis-sg:  pred_prob[%s] = %s ' % (ss, pred_prob[ss]))
                # print(' pred-BN-(1-cos): wst_pre[%s] = %s ' % (ss, wst_pre[ss]))
                # print(' pred-BN-cos:     wst_d[%s] = %s ' % (ss, wst_d[ss]))
                #
                # for temp_i in range(nb):
                #     temp_s = ((nb - 1) + (nb - 1 - (temp_i - 1))) * temp_i * 0.5
                #     # print("\n temp_s == ", temp_s)
                #     if temp_s > ss:
                #         temp_ss = ((nb - 1) + (nb - 1 - (temp_i - 2))) * (temp_i - 1) * 0.5
                #         # print("\n temp_ss == ", temp_ss)
                #         temp_j = int(ss - temp_ss + temp_i)
                #         # print("\n temp_j == ", temp_j)
                #         print("\n ************************ N0. %s************************************ " % (temp_c))
                #         print(' ----------- Cosine(a,b) -----------')
                #         print(' Have %s: \n nij-wst_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                #                                                            wst_temp[temp_i - 1, temp_j]))
                #         print(' ----------- |a|*|b| -----------')
                #         print(' Have %s: \n nij-wst_l2_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                #                                                               wst_l2_temp[temp_i - 1, temp_j]))
                #         print(' ----------- Sum（a*b） -----------')
                #         print(' Have %s: \n nij-wst_d_temp[%s][%s] = %s ' % (key, temp_i - 1, temp_j,
                #                                                              wst_d_temp[temp_i - 1, temp_j]))
                #         print(' ----------- After BN -----------')
                #         print(' Have %s: \n ni-wst_d_ps[%s] = %s ' % (key, temp_i - 1, wst_d_ps[temp_i - 1, 5:]))
                #         print(' nj-wst_d_ps[%s] = %s ' % (temp_j, wst_d_ps[temp_j, 5:]))
                #         print(' ----------- Before BN -----------')
                #         print(' Have %s: \n ni-pred[%s] = %s ' % (key, temp_i - 1, pred[temp_i - 1, 5:]))
                #         print(' nj-pred[%s] = %s ' % (temp_j, pred[temp_j, 5:]))
                #         break


