import numpy as np
import matplotlib.pyplot as plt
import argparse
import math

def get_dy0(x, A, B, C, D, a1, a2, a3):
    xx = [0] * len(x)
    yy = [0] * len(x)
    yy_1 = [0] * len(x)
    for i in range(0, len(x)):
        if x[i] == 0:
            xx[i] = 0.0001
        elif x[i] == 1:
            xx[i] = 0.9999
        else:
            xx[i] = x[i]

        # yy[i] = - math.pow((xx[i]), 2) * 0.75 * math.log(1 - xx[i])
        # yy[i] = (a1*xx[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*xx[i]) + B)/A   #贝塞尔曲线(二阶)
        # yy[i] = (a1*xx[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*xx[i]) + B)/A - math.pow((xx[i]), 2)*0.75*math.log(1 - xx[i])
        # yy[i] = - math.pow((1 - xx[i]), 2)*0.25*math.log(xx[i]) - math.pow((xx[i]), 2)*0.75*math.log(1 - xx[i])

        # yy_1[i] = - math.pow((xx[i]), 2)*0.75*math.log(1 - xx[i])
        # yy_1[i] = (a1*xx[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*xx[i]) + B)/A   #贝塞尔曲线(二阶)
        # yy_1[i] = (a1*xx[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*xx[i]) + B)/A - math.pow((xx[i]), 2)*0.75*math.log(1 - xx[i])
        yy_1[i] = - math.pow((1 - xx[i]), 2)*0.1*math.log(xx[i]) - math.pow((xx[i]), 2)*0.75*math.log(1 - xx[i])   #较好1

    return xx, yy, yy_1

def get_dy1(x, C, D, a1, a2, a3, y2, y0):
    X = [0] * len(x)
    Y = [0] * len(x)
    Y1 = [0] * len(x)
    for i in range(0, len(x)):
        if x[i] == 0:
            X[i] = 0.0001
        elif x[i] == 1:
            X[i] = 0.9999
        else:
            X[i] = x[i]

        # Y[i] = - math.pow((1 - X[i]), 2)*0.25*math.log(X[i])
        # Y[i] = (a1*X[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*X[i]))*(y0 - y2) + y2   #贝塞尔曲线(二阶)
        # Y[i] = (a1*X[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*X[i]))*(y0 - y2) + y2 - math.pow((1 - X[i]), 2)*0.25*math.log(X[i])

        Y1[i] = - math.pow((1 - X[i]), 2)*0.25*math.log(X[i])   #较好1
        # Y1[i] = (a1*X[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*X[i]))*(y0 - y2) + y2   #贝塞尔曲线(二阶)
        # Y1[i] = (a1*X[i] + a2 + a3*math.sqrt(C*C + (1 - 2*C)*X[i]))*(y0 - y2) + y2 - math.pow((1 - X[i]), 2)*0.25*math.log(X[i])

    return X, Y, Y1

def get_parameter(x0, x1, x2, y0, y1, y2, X0, X1, X2, Y0, Y1, Y2):
    # dy=0的情况
    C_dy0 = (x1 - x0)/(x2 - x0)
    D_dy0 = (y1 - y0)/(y2 - y0)
    A_dy0 = C_dy0/((y2 - y0)*(C_dy0 - D_dy0))
    B_dy0 = A_dy0*y0
    a1_dy0 = D_dy0/(C_dy0 - D_dy0) + 1/(1 - 2*C_dy0)
    a2_dy0 = 2*math.pow(C_dy0, 2)/math.pow(1 - 2*C_dy0, 2)
    a3_dy0 = - 2*C_dy0/math.pow(1 - 2*C_dy0, 2)

    # dy=1的情况
    C_dy1 = (X1 - X0) / (X2 - X0)
    D_dy1 = (Y1 - Y2) / (Y0 - Y2)
    a1_dy1 = (1 - 2*D_dy1) / (1 - 2*C_dy1)
    a2_dy1 = (1 - 2*C_dy1 - 2*D_dy1*C_dy1 + 2*math.pow(C_dy1, 2)) / math.pow(1 - 2*C_dy1, 2)
    a3_dy1 = (2*D_dy1 - 2 + 2*C_dy1) / math.pow(1 - 2*C_dy1, 2)

    return A_dy0, B_dy0, C_dy0, D_dy0, a1_dy0, a2_dy0, a3_dy0, C_dy1, D_dy1, a1_dy1, a2_dy1, a3_dy1

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--p0_dy0', type=int, default=[0, 2])
    parser.add_argument('--p1_dy0', type=int, default=[0.1, -2])
    parser.add_argument('--p2_dy0', type=int, default=[1, 4.01])
    parser.add_argument('--P0_dy1', type=int, default=[0, 4])
    parser.add_argument('--P1_dy1', type=int, default=[0.1, -0.1])
    parser.add_argument('--P2_dy1', type=int, default=[1, 0.01])

    opt = parser.parse_args()

    # print(opt.p0[-1])
    x0, y0 = opt.p0_dy0
    x1, y1 = opt.p1_dy0
    x2, y2 = opt.p2_dy0

    X0, Y0 = opt.P0_dy1
    X1, Y1 = opt.P1_dy1
    X2, Y2 = opt.P2_dy1

    A_dy0, B_dy0, C_dy0, D_dy0, a1_dy0, a2_dy0, a3_dy0, C_dy1, D_dy1, a1_dy1, a2_dy1, a3_dy1 \
        = get_parameter(x0, x1, x2, y0, y1, y2, X0, X1, X2, Y0, Y1, Y2)

    x = np.linspace(0, 1, 1001)

    x_dy0, y_dy0, y1_dy0 = get_dy0(x, A_dy0, B_dy0, C_dy0, D_dy0, a1_dy0, a2_dy0, a3_dy0)
    X_dy1, Y_dy1, Y1_dy1 = get_dy1(x, C_dy1, D_dy1, a1_dy1, a2_dy1, a3_dy1, Y2, Y0)

    # dy0情况
    print("y_dy0最小值：%s， 索引x：%s"%(min(y_dy0),  x_dy0[y_dy0.index(min(y_dy0))]))
    print("y1_dy0最小值：%s， 索引x：%s"%(min(y1_dy0),  x_dy0[y1_dy0.index(min(y1_dy0))]))

    # dy1情况
    print("Y_dy1最小值：%s， 索引x：%s"%(min(Y_dy1),  X_dy1[Y_dy1.index(min(Y_dy1))]))
    print("Y1_dy1最小值：%s， 索引x：%s"%(min(Y1_dy1),  X_dy1[Y1_dy1.index(min(Y1_dy1))]))

    for i in range(0, 1000):
        if abs(y1_dy0[i] - Y1_dy1[i]) < 0.001:
            print('交叉点x = %s，y = %s'%(x_dy0[i], y1_dy0[i]))

    # plt.plot(x_dy0, y_dy0, 'r', linewidth=1)       # 最终拟合的贝塞尔曲线
    plt.plot(x_dy0, y1_dy0, 'y', linewidth=1)       # 最终拟合的贝塞尔曲线
    # plt.plot(X_dy1, Y_dy1, 'b', linewidth=1)       # 最终拟合的贝塞尔曲线
    plt.plot(X_dy1, Y1_dy1, 'g', linewidth=1)       # 最终拟合的贝塞尔曲线
    # plt.scatter(x_dy0[:], y_dy0[:], 1, "red")     #散点图,表示采样点
    plt.scatter(x_dy0[:], y1_dy0[:], 1, "yellow")     #散点图,表示采样点
    # plt.scatter(X_dy1[:], Y_dy1[:], 1, "blue")     #散点图,表示采样点
    plt.scatter(X_dy1[:], Y1_dy1[:], 1, "green")     #散点图,表示采样点
    plt.show()