import torch
import torch.nn as nn
import numpy as np
import cv2
import time
import matplotlib.pyplot as plt


# a = [[0,0,0,0,0, 0.09850, 0.07381, 0.09498, 0.06709, 0.04420, 0.09838, -0.07472, 0.09704, 0.10511],
#         [0,0,0,0,0, 0.12517, 0.08320, 0.05671, 0.08169, 0.03311, 0.11040, -0.07720, 0.10210, 0.09071],
#         [0,0,0,0,0, 0.08007, 0.08626, 0.07167, 0.08454, 0.03979, 0.07875, -0.10219, 0.07290, 0.08353],
#         [0,0,0,0,0, 0.04756, 0.09166, 0.06956, 0.15907, 0.09441, 0.09501, -0.06844, 0.05258, 0.05222],
#         [0,0,0,0,0, 0.04652, 0.06091, 0.04461, 0.16689, 0.05162, 0.07181, -0.07676, 0.04061, 0.05499],
#         [0,0,0,0,0, 0.05683, 0.05448, 0.02966, 0.15260, 0.12075, 0.06657, -0.06459, 0.04622, 0.05860],
#         [0,0,0,0,0, 0.06292, 0.09569, 0.04262, 0.06236, 0.07043, 0.06593, -0.02867, 0.10360, 0.08807],
#         [0,0,0,0,0, 0.16726, 0.07523, 0.04722, 0.05191, 0.04980, 0.06777, -0.05449, 0.04418, 0.07774]]

# a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
#         [0,0,0,0,0, -7.21984,   4.77345, -17.08892, -19.98676,  -6.65397, -10.56651, -15.72788,  -4.23451, -11.30104],
#         [0,0,0,0,0, 7.56419,  -5.15149,  -5.77484, -12.28424, -10.24066,  -8.07071,  -8.84589,  -9.55158, -11.28349],
#         [0,0,0,0,0, -4.41505, -11.84778,   5.30759,  -4.94095,  -8.08970, -14.26806,  -8.22902, -14.45283, -11.11575],
#         [0,0,0,0,0, -4.85881, -12.93312,   6.13393,  -4.75256,  -8.30600, -15.60899,  9.30687, -15.94003, -11.89202],
#         [0,0,0,0,0, -9.16481,   4.85175, -18.92580, -18.22492, -10.74850,  -7.82335, -12.80189,  -2.70451, -12.57616],
#         [0,1,0,0,0, -2.91972e+00, -2.20095e+00, 3.48893e+00, -2.59476e+00, -2.79146e+00, -2.14687e+00, -1.84426e+00, -2.32959e+00, -2.42477e+00],
#         [0,0,1,0,0, 2.26935e+00, -2.67812e+00, -3.04553e+00, -3.46773e+00, -2.81742e+00, -2.19005e+00, -2.42943e+00, -2.88006e+00, -2.69119e+00],
#         [0,0,0,1,0, -2.82522e+00, -2.65070e+00, -3.47379e+00, -1.19072e+00, -2.82531e+00, -2.53264e+00, -2.48552e+00, -3.06835e+00, -2.50541e+00],
#         [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
#         [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
#         [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
#         [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
#         [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00],
#         [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
#         [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]
#
# at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [1., 0., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 1., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [1., 0., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 1., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 1., 0., 0., 0., 0., 0., 0., 0.],
#         [1., 0., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 1., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 1., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 0., 0., 1., 0., 0., 0.],
#         [0., 0., 0., 0., 0., 0., 0., 1., 0.]]

a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
        [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
        [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
        [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
        [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
        [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00],
        [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
        [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 1., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 0., 1., 0.]]


'''############################ 详细版 ############################'''
# c0_pre_r = 0
# c0_pre_w = 0
# c1_pre_r = 0
# c1_pre_w = 0
#
# ps = torch.tensor(a, requires_grad=True)
# t = torch.tensor(at)
# nb = ps.shape[0]
# nc = 9
#
#
# check_ps = ps[:, 5:].detach()
# check_ps = check_ps.sigmoid()  # sigmoid???
#
# check_t = t.detach()
#
# _, ct = torch.max(check_t, dim=1)
# print('ct = ', ct)
#
# _, cps = torch.max(check_ps, dim=1)
# print('cps = ', cps)
#
# '''
# pre与label对比mask：
# -4 —— 2个pre都与各自label不符
# -1 —— 1个pre都与各自label不符
# 2  —— 2个pre都与各自label相符
# '''
# ct = ct.to(float)
# cps = cps.to(float)
# label_pre_check = ct - cps
# # print('label_pre_check = ', label_pre_check)
# ones_lpc = torch.ones_like(label_pre_check)
# others_lpc = 0.0 - 2 * ones_lpc
# label_pre_check = torch.where(label_pre_check == 0.0, ones_lpc, others_lpc)
# # print('label_pre_check = ', label_pre_check)
# temp_lpc = label_pre_check.unsqueeze(dim=1)
# # print('temp_lpc = ', temp_lpc)
# temp_lpc = temp_lpc.expand(temp_lpc.shape[0], temp_lpc.shape[0])
# # print('temp_lpc = ', temp_lpc)
# label_pre_check = label_pre_check + temp_lpc
# # print('label_pre_check = ', label_pre_check)
# diag_lpc = torch.diag(label_pre_check)
# e_diag_lpc = torch.diag_embed(diag_lpc)
# # print('e_diag_lpc = ', e_diag_lpc)
# label_pre_check = label_pre_check - e_diag_lpc
# print('label_pre_check = ', label_pre_check)
#
# '''
# 基于label的对比mask：
# 0 —— 自身比较，即对角线，略
# 1 —— 通过label识别，2个label同类
# 10 —— 通过label识别，2个label不同类
# '''
# # label_sd = torch.pdist(check_t, p=1) / 2
# # print('label_sd = ', label_sd)
# label_sdn = torch.norm(check_t[:, None] - check_t, dim=2, p=1) / 2
# # print('label_sdn = ', label_sdn)
# ones_ln = torch.ones_like(label_sdn)
# others_ln = 10 * ones_ln
# label_sdn = torch.where(label_sdn > 0.0, others_ln, ones_ln)
# # print('pred_c_sdn = ', pred_c_sdn)
# diag_l = torch.diag(label_sdn)
# e_diag_l = torch.diag_embed(diag_l)
# label_sdn = label_sdn - e_diag_l
# print('label_sdn = ', label_sdn)
# '''
# 基于pre的对比mask：
# 0 —— 自身比较，即对角线，略
# 1 —— 通过pre识别，2个pre同类
# 2 —— 通过pre识别，2个pre不同类
# '''
# temp_cps = cps.unsqueeze(1)
# # pred_c_sd = torch.pdist(temp_cps, p=1)
# # # print('pred_c_sd = ', pred_c_sd)
# # ones = torch.ones_like(pred_c_sd)
# # others = 2 * ones
# # pred_c_sd = torch.where(pred_c_sd > 0.0, others, ones)
# # print('pred_c_sd = ', pred_c_sd)
#
#
# pred_c_sdn = torch.norm(temp_cps[:, None] - temp_cps, dim=2, p=1)
# # print('pred_c_sdn = ', pred_c_sdn)
# ones_n = torch.ones_like(pred_c_sdn)
# others_n = 2 * ones_n
# pred_c_sdn = torch.where(pred_c_sdn > 0.0, others_n, ones_n)
# # print('pred_c_sdn = ', pred_c_sdn)
# diag = torch.diag(pred_c_sdn)
# e_diag = torch.diag_embed(diag)
# pred_c_sdn = pred_c_sdn - e_diag
# print('pred_c_sdn = ', pred_c_sdn)
#
#
# sdn_lp_check = label_sdn * pred_c_sdn
# print('sdn_lp_check = ', sdn_lp_check)
# sdn_lp_check = sdn_lp_check.cpu().numpy()
# aaa0 = np.argwhere(sdn_lp_check == 1.0)
# bbb0 = np.argwhere(sdn_lp_check == 2.0)
# c0_pre_r = aaa0.shape[0] / 2
# c0_pre_w = bbb0.shape[0] / 2
# aaa1 = np.argwhere(sdn_lp_check == 20.0)
# bbb1 = np.argwhere(sdn_lp_check == 10.0)
# c1_pre_r = aaa1.shape[0] / 2
# c1_pre_w = bbb1.shape[0] / 2
# print("\n c0_pre_r \n", c0_pre_r)
# print("\n c0_pre_w \n", c0_pre_w)
# print("\n c1_pre_r \n", c1_pre_r)
# print("\n c1_pre_w \n", c1_pre_w)
#
#
#
#
#
# sta_lpc_check = label_pre_check * sdn_lp_check
# print('sta_check = ', sta_lpc_check)
# sta_lpc_check = sta_lpc_check.cpu().numpy()
# aaa0c = np.argwhere(sta_lpc_check == 2.0)
# bbb0c = sta_lpc_check <= -1.0
# ccc0c = sta_lpc_check >= -8.0
# temp_bc0c = bbb0c & ccc0c
# temp_0c = np.argwhere(temp_bc0c == True)
# c0_pre_rc = aaa0c.shape[0] / 2
# c0_pre_wc = temp_0c.shape[0] / 2
#
# aaa1c = np.argwhere(sta_lpc_check == 40.0)
# bbb1c = sta_lpc_check <= -10.0
# ccc1c = sta_lpc_check >= -80.0
# temp_bc1c = bbb1c & ccc1c
# temp_1c = np.argwhere(temp_bc1c == True)
# c1_pre_rc = aaa1c.shape[0] / 2
# c1_pre_wc = temp_1c.shape[0] / 2
# print("\n c0_pre_rc \n", c0_pre_rc)
# print("\n c0_pre_wc \n", c0_pre_wc)
# print("\n c1_pre_rc \n", c1_pre_rc)
# print("\n c1_pre_wc \n", c1_pre_wc)



'''############################ 精简版 ############################'''
def remove_diag_effect(tensor_in):
        diag = torch.diag(tensor_in)
        e_diag = torch.diag_embed(diag)
        tensor_in = tensor_in - e_diag
        return tensor_in

c0_pre_r, c0_pre_rc = 0, 0
c0_pre_w, c0_pre_wc = 0, 0
c1_pre_r, c1_pre_rc = 0, 0
c1_pre_w, c1_pre_wc = 0, 0

ps = torch.tensor(a, requires_grad=True)
t = torch.tensor(at)
nb = ps.shape[0]
nc = 9

check_ps = ps[:, 5:].detach()
check_ps = check_ps.sigmoid()  # sigmoid
check_t = t.detach()
_, ct = torch.max(check_t, dim=1)
_, cps = torch.max(check_ps, dim=1)
ct = ct.to(float)
cps = cps.to(float)


'''
pre与label对比mask：
-4 —— 2个pre都与各自label不符
-1 —— 1个pre都与各自label不符
2  —— 2个pre都与各自label相符
'''
label_pre_check = ct - cps
ones_lpc = torch.ones_like(label_pre_check)
others_lpc = 0.0 - 2 * ones_lpc
label_pre_check = torch.where(label_pre_check == 0.0, ones_lpc, others_lpc)

temp_lpc = label_pre_check.unsqueeze(dim=1)
temp_lpc = temp_lpc.expand(temp_lpc.shape[0], temp_lpc.shape[0])
label_pre_check = label_pre_check + temp_lpc
# 去对角线元素影响
label_pre_check = remove_diag_effect(label_pre_check)
print('label_pre_check = ', label_pre_check)


'''
基于label的对比mask：
0 —— 自身比较，即对角线，略
1 —— 通过label识别，2个label同类
10 —— 通过label识别，2个label不同类
'''
label_sd = torch.norm(check_t[:, None] - check_t, dim=2, p=1) / 2
ones_l = torch.ones_like(label_sd)
others_l = 10 * ones_l
label_sd = torch.where(label_sd > 0.0, others_l, ones_l)
# 去对角线元素影响
label_sd = remove_diag_effect(label_sd)
print('label_sd = ', label_sd)


'''
基于pre的对比mask：
0 —— 自身比较，即对角线，略
1 —— 通过pre识别，2个pre同类
2 —— 通过pre识别，2个pre不同类
'''
temp_cps = cps.unsqueeze(1)
pred_sd = torch.norm(temp_cps[:, None] - temp_cps, dim=2, p=1)
ones_p = torch.ones_like(pred_sd)
others_p = 2 * ones_p
pred_sd = torch.where(pred_sd > 0.0, others_p, ones_p)
# 去对角线元素影响
pred_sd = remove_diag_effect(pred_sd)
print('pred_sd = ', pred_sd)


def statis_rw(numpy_in, r=None, w=None):
        aaa0 = np.argwhere(numpy_in == r)
        bbb0 = np.argwhere(numpy_in == w)
        pre_r = aaa0.shape[0] / 2
        pre_w = bbb0.shape[0] / 2
        return pre_r, pre_w


def statis_rw_c(numpy_in, r=None, w1=None, w2=None):
        aaa0c = np.argwhere(numpy_in == r)
        bbb0c = numpy_in <= w1
        ccc0c = numpy_in >= w2
        temp_bc0c = bbb0c & ccc0c
        temp_0c = np.argwhere(temp_bc0c == True)
        pre_r = aaa0c.shape[0] / 2
        pre_w = temp_0c.shape[0] / 2
        return pre_r, pre_w


sd_lp_check = label_sd * pred_sd
# print('sd_lp_check = ', sd_lp_check)
sd_lp_check = sd_lp_check.cpu().numpy()
c0_pre_r, c0_pre_w = statis_rw(sd_lp_check, r=1.0, w=2.0)
c1_pre_r, c1_pre_w = statis_rw(sd_lp_check, r=20.0, w=10.0)
print("\n c0_pre_r = %s\n c0_pre_w = %s\n c1_pre_r = %s\n c1_pre_w = %s"
      % (c0_pre_r, c0_pre_w, c1_pre_r, c1_pre_w))

sta_lpc_check = label_pre_check * sd_lp_check
# print('sta_check = ', sta_lpc_check)
sta_lpc_check = sta_lpc_check.cpu().numpy()
c0_pre_rc, c0_pre_wc = statis_rw_c(sta_lpc_check, r=2.0, w1=-1.0, w2=-8.0)
c1_pre_rc, c1_pre_wc = statis_rw_c(sta_lpc_check, r=40.0, w1=-10.0, w2=-80.0)
print("\n c0_pre_rc = %s\n c0_pre_wc = %s\n c1_pre_rc = %s\n c1_pre_wc = %s"
      % (c0_pre_rc, c0_pre_wc, c1_pre_rc, c1_pre_wc))






