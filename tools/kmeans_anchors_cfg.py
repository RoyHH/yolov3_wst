from utils.utils import *

if __name__ == '__main__':
    kmean_anchors(path='../data/IGBT_DF2021_train.txt', n=9, img_size=(640, 640), thr=0.4, gen=1000)


# 原始：10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
#####################################################################################################
''' iou_t = 0.2, img_size=(320, 640) logs
0.20 iou_thr: 0.998 best possible recall, 4.22 anchors > thr
n=9, img_size=(320, 640), IoU_all=0.251/0.681-mean/best, IoU>thr=0.451-mean: 4,5,  7,7,  10,12,  18,15,  14,22,  31,22,  24,37,  49,47,  87,92
'''
''' iou_t = 0.2, img_size=(640, 640) logs
0.20 iou_thr: 0.998 best possible recall, 4.19 anchors > thr
n=9, img_size=(640, 640), IoU_all=0.249/0.684-mean/best, IoU>thr=0.449-mean: 6,7,  10,11,  17,17,  20,31,  30,25,  47,36,  32,62,  70,64,  119,122
'''

#####################################################################################################
''' iou_t = 0.25, img_size=(320, 640) logs
0.25 iou_thr: 0.995 best possible recall, 3.49 anchors > thr
n=9, img_size=(320, 640), IoU_all=0.243/0.681-mean/best, IoU>thr=0.490-mean: 4,5,  7,7,  10,11,  15,14,  17,27,  28,19,  36,39,  63,56,  101,107
'''
''' iou_t = 0.25, img_size=(640, 640) logs
0.25 iou_thr: 0.994 best possible recall, 3.60 anchors > thr
n=9, img_size=(640, 640), IoU_all=0.250/0.684-mean/best, IoU>thr=0.489-mean: 6,6,  9,10,  15,16,  22,22,  21,36,  42,31,  38,63,  66,63,  119,120
'''

#####################################################################################################
''' iou_t = 0.3, img_size=(320, 640) logs
0.30 iou_thr: 0.988 best possible recall, 2.99 anchors > thr
n=9, img_size=(320, 640), IoU_all=0.242/0.680-mean/best, IoU>thr=0.522-mean: 4,4,  6,7,  11,11,  15,17,  28,19,  18,33,  42,31,  48,55,  91,86
'''
''' iou_t = 0.3, img_size=(640, 640) logs
0.30 iou_thr: 0.987 best possible recall, 3.14 anchors > thr
n=9, img_size=(640, 640), IoU_all=0.253/0.686-mean/best, IoU>thr=0.524-mean: 6,7,  10,10,  14,16,  23,20,  21,34,  40,28,  39,53,  66,61,  122,123
'''

#####################################################################################################
''' iou_t = 0.35, img_size=(320, 640) logs
0.35 iou_thr: 0.974 best possible recall, 2.62 anchors > thr
n=9, img_size=(320, 640), IoU_all=0.247/0.681-mean/best, IoU>thr=0.556-mean: 4,4,  7,7,  10,12,  17,13,  15,21,  34,23,  23,38,  52,51,  91,98
'''
''' iou_t = 0.35, img_size=(640, 640) logs
0.35 iou_thr: 0.975 best possible recall, 2.65 anchors > thr
n=9, img_size=(640, 640), IoU_all=0.251/0.684-mean/best, IoU>thr=0.554-mean: 6,6,  10,11,  16,16,  25,22,  19,35,  54,28,  36,45,  67,63,  123,119
'''

#####################################################################################################
''' iou_t = 0.4, img_size=(320, 640) logs
0.40 iou_thr: 0.960 best possible recall, 2.18 anchors > thr
n=9, img_size=(320, 640), IoU_all=0.244/0.679-mean/best, IoU>thr=0.587-mean: 4,4,  7,8,  10,15,  14,10,  18,19,  38,23,  24,38,  52,50,  94,102
'''
''' iou_t = 0.4, img_size=(640, 640) logs
0.40 iou_thr: 0.957 best possible recall, 2.21 anchors > thr
n=9, img_size=(640, 640), IoU_all=0.248/0.683-mean/best, IoU>thr=0.590-mean: 5,6,  10,10,  13,16,  21,19,  21,37,  37,25,  40,57,  73,63,  116,122
'''

