import torch
import torch.nn as nn
import numpy as np
import cv2
import time

# # Adapted from https://github.com/gpeyre/SinkhornAutoDiff
# class SinkhornDistance(nn.Module):
#     r"""
#     Given two empirical measures each with :math:`P_1` locations
#     :math:`x\in\mathbb{R}^{D_1}` and :math:`P_2` locations :math:`y\in\mathbb{R}^{D_2}`,
#     outputs an approximation of the regularized OT cost for point clouds.
#     Args:
#         eps (float): regularization coefficient
#         max_iter (int): maximum number of Sinkhorn iterations
#         reduction (string, optional): Specifies the reduction to apply to the output:
#             'none' | 'mean' | 'sum'. 'none': no reduction will be applied,
#             'mean': the sum of the output will be divided by the number of
#             elements in the output, 'sum': the output will be summed. Default: 'none'
#     Shape:
#         - Input: :math:`(N, P_1, D_1)`, :math:`(N, P_2, D_2)`
#         - Output: :math:`(N)` or :math:`()`, depending on `reduction`
#     """
#     def __init__(self, eps, max_iter, reduction='none'):
#         super(SinkhornDistance, self).__init__()
#         self.eps = eps
#         self.max_iter = max_iter
#         self.reduction = reduction
#
#     def forward(self, x, y):
#         # The Sinkhorn algorithm takes as input three variables :
#         C = self._cost_matrix(x, y)  # Wasserstein cost function
#         x_points = x.shape[-2]
#         y_points = y.shape[-2]
#         if x.dim() == 2:
#             batch_size = 1
#         else:
#             batch_size = x.shape[0]
#
#         # both marginals are fixed with equal weights
#         mu = torch.empty(batch_size, x_points, dtype=torch.float,
#                          requires_grad=False).fill_(1.0 / x_points).squeeze()
#         nu = torch.empty(batch_size, y_points, dtype=torch.float,
#                          requires_grad=False).fill_(1.0 / y_points).squeeze()
#
#         u = torch.zeros_like(mu)
#         v = torch.zeros_like(nu)
#         # To check if algorithm terminates because of threshold
#         # or max iterations reached
#         actual_nits = 0
#         # Stopping criterion
#         thresh = 1e-1
#
#         # Sinkhorn iterations
#         for i in range(self.max_iter):
#             u1 = u  # useful to check the update
#             u = self.eps * (torch.log(mu+1e-8) - torch.logsumexp(self.M(C, u, v), dim=-1)) + u
#             v = self.eps * (torch.log(nu+1e-8) - torch.logsumexp(self.M(C, u, v).transpose(-2, -1), dim=-1)) + v
#             err = (u - u1).abs().sum(-1).mean()
#
#             actual_nits += 1
#             if err.item() < thresh:
#                 break
#
#         U, V = u, v
#         # Transport plan pi = diag(a)*K*diag(b)
#         pi = torch.exp(self.M(C, U, V))
#         # Sinkhorn distance
#         cost = torch.sum(pi * C, dim=(-2, -1))
#
#         if self.reduction == 'mean':
#             cost = cost.mean()
#         elif self.reduction == 'sum':
#             cost = cost.sum()
#
#         return cost, pi, C
#
#     def M(self, C, u, v):
#         "Modified cost for logarithmic updates"
#         "$M_{ij} = (-c_{ij} + u_i + v_j) / \epsilon$"
#         return (-C + u.unsqueeze(-1) + v.unsqueeze(-2)) / self.eps
#
#     @staticmethod
#     def _cost_matrix(x, y, p=2):
#         "Returns the matrix of $|x_i-y_j|^p$."
#         x_col = x.unsqueeze(-2)
#         y_lin = y.unsqueeze(-3)
#         C = torch.sum((torch.abs(x_col - y_lin)) ** p, -1)
#         return C
#
#     @staticmethod
#     def ave(u, u1, tau):
#         "Barycenter subroutine, used by kinetic acceleration through extrapolation."
#         return tau * u + (1 - tau) * u1


# tensor([0.58298, 0.58845, 3.40034, 3.90483, 5.25154, 2.54935, 2.12466, 0.72693, 3.88741, 3.69844, 5.13168, 2.80637, 1.84464, 2.28814, 2.09636, 3.78161, 2.86541, 2.02431, 1.02776, 1.41432, 3.39569, 4.57124, 1.17012, 4.01562, 4.02502, 3.53929, 4.27503, 2.76169], device='cuda:0', dtype=torch.float64,
#        grad_fn=<CopyBackwards>)

# # v = [0.09850, 0.07381, 0.09498, 0.06709, 0.04420, 0.09838, 0.07472, 0.09704, 0.10511]
# # u = [0.12517, 0.08320, 0.05671, 0.08169, 0.03311, 0.11040, 0.07720, 0.10210, 0.09071]
# v = [0.09850, 0.07381, 0.09498, 0.06709]
# u = [0.12517, 0.08320, 0.05671, 0.08169]
# p = 1
#
# u_weights = None
# v_weights = None
#
# # 初始化
# v_n = np.asarray(v, dtype=float)
# u_n = np.asarray(u, dtype=float)
# v_t = torch.tensor(v, dtype=torch.float32)
# u_t = torch.tensor(u, dtype=torch.float32)
#
# # 排序
# u_sorter = np.argsort(u_n)
# v_sorter = np.argsort(v_n)
# print("\n u_sorter ===>", u_sorter)
# print("\n v_sorter ===>", v_sorter)
# # u_t_n = u_t.detach().numpy()
# # u_sorter_t = np.argsort(u_t_n)
# # v_t_n = v_t.detach().numpy()
# # v_sorter_t = np.argsort(v_t_n)
#
# u_t_n = u_t.detach()
# u_sorter_t = torch.argsort(u_t_n)
# v_t_n = v_t.detach()
# v_sorter_t = torch.argsort(v_t_n)
# print("\n u_sorter_t ===>", u_sorter_t)
# print("\n v_sorter_t ===>", v_sorter_t)
#
#
# # concat操作
# values = np.concatenate((v, u))
# print("\n va_nc ===>", values)
# values_t = torch.cat([v_t, u_t])
# print("\n va_tc ===>", values_t)
#
# # 全排列
# values.sort(kind='mergesort')
# print("\n b ==>", values)
# bb, bb_sort = torch.sort(values_t)
# print("\n bb ===>", bb)
#
# # 排序差值
# deltas_n = np.diff(values)
# print("\n deltas_n ===>", deltas_n)
# deltas_t = torch.zeros((bb.shape[0] - 1), dtype=torch.float32)
# for i in range(bb.shape[0] - 1):
#     deltas_t[i] = bb[i + 1] - bb[i]
#
# # 插值排序
# u_cdf_indices = u_n[u_sorter].searchsorted(values[:-1], 'right')
# v_cdf_indices = v_n[v_sorter].searchsorted(values[:-1], 'right')
# print("\n u_cdf_indices ===>", u_cdf_indices)
# print("\n v_cdf_indices ===>", v_cdf_indices)
# u_cdf_indices_t = torch.searchsorted(u_t[u_sorter_t], bb[:-1], right=True)
# v_cdf_indices_t = torch.searchsorted(v_t[v_sorter_t], bb[:-1], right=True)
# print("\n u_cdf_indices_t ===>", u_cdf_indices_t)
# print("\n v_cdf_indices_t ===>", v_cdf_indices_t)
#
#
# v_cdf = v_cdf_indices / v_n.size
# u_cdf = u_cdf_indices / u_n.size
# print("\n v_cdf ===>", v_cdf)
# print("\n u_cdf ===>", u_cdf)
# v_cdf_t = torch.true_divide(v_cdf_indices_t, v_t.shape[0])
# u_cdf_t = torch.true_divide(u_cdf_indices_t, u_t.shape[0])
# print("\n v_cdf_t ===>", v_cdf_t)
# print("\n u_cdf_t ===>", u_cdf_t)
#
#
# if p == 1:
#     print("\n p=1时，结果为",
#           np.sum(np.multiply(np.abs(u_cdf - v_cdf), deltas_n)))
#     # np.sum(np.multiply(np.abs(u_cdf - v_cdf), deltas_n))
#     print("\n p=1时，torch结果为",
#           (torch.abs(u_cdf_t - v_cdf_t) * deltas_t).sum())
#     # np.sum(np.multiply(np.abs(u_cdf - v_cdf), deltas_n))
#
# elif p == 2:
#     print("\n p=2时，结果为",
#           np.sqrt(np.sum(np.multiply(np.square(u_cdf - v_cdf), deltas_n))))
# else:
#     print("\n 其它p，结果为",
#           np.power(np.sum(np.multiply(np.power(np.abs(u_cdf - v_cdf), p), deltas_n)), 1 / p))



# a = [[0,0,0,0,0, 0.09850, 0.07381, 0.09498, 0.06709, 0.04420, 0.09838, 0.07472, 0.09704, 0.10511],
#         [0,0,0,0,0, 0.12517, 0.08320, 0.05671, 0.08169, 0.03311, 0.11040, 0.07720, 0.10210, 0.09071],
#         [0,0,0,0,0, 0.08007, 0.08626, 0.07167, 0.08454, 0.03979, 0.07875, 0.10219, 0.07290, 0.08353],
#         [0,0,0,0,0, 0.04756, 0.09166, 0.06956, 0.15907, 0.09441, 0.09501, 0.06844, 0.05258, 0.05222],
#         [0,0,0,0,0, 0.04652, 0.06091, 0.04461, 0.16689, 0.05162, 0.07181, 0.07676, 0.04061, 0.05499],
#         [0,0,0,0,0, 0.05683, 0.05448, 0.02966, 0.15260, 0.12075, 0.06657, 0.06459, 0.04622, 0.05860],
#         [0,0,0,0,0, 0.06292, 0.09569, 0.04262, 0.06236, 0.07043, 0.06593, 0.02867, 0.10360, 0.08807],
#         [0,0,0,0,0, 0.16726, 0.07523, 0.04722, 0.05191, 0.04980, 0.06777, 0.05449, 0.04418, 0.07774]]

# a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
#         [0,0,0,0,0, -7.21984,   4.77345, -17.08892, -19.98676,  -6.65397, -10.56651, -15.72788,  -4.23451, -11.30104],
#         [0,0,0,0,0, 7.56419,  -5.15149,  -5.77484, -12.28424, -10.24066,  -8.07071,  -8.84589,  -9.55158, -11.28349],
#         [0,0,0,0,0, -4.41505, -11.84778,   5.30759,  -4.94095,  -8.08970, -14.26806,  -8.22902, -14.45283, -11.11575],
#         [0,0,0,0,0, -4.85881, -12.93312,   6.13393,  -4.75256,  -8.30600, -15.60899,  9.30687, -15.94003, -11.89202],
#         [0,0,0,0,0, -9.16481,   4.85175, -18.92580, -18.22492, -10.74850,  -7.82335, -12.80189,  -2.70451, -12.57616]]

a =[[0,1,0,0,0, -2.91972e+00, -2.20095e+00, -3.48893e+00, -2.59476e+00, -2.79146e+00, -2.14687e+00, -1.84426e+00, -2.32959e+00, -2.42477e+00],
        [0,0,1,0,0, 2.26935e+00, -2.67812e+00, -3.04553e+00, -3.46773e+00, -2.81742e+00, -2.19005e+00, -2.42943e+00, -2.88006e+00, -2.69119e+00],
        [0,0,0,1,0, -2.82522e+00, -2.65070e+00, -3.47379e+00, -3.19072e+00, -2.82531e+00, -2.53264e+00, -2.48552e+00, -3.06835e+00, -2.50541e+00],
        [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
        [0,1,0,0,0, -2.36682e+00, 2.49370e+00, -2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
        [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
        [0,0,0,0,1, -2.36243e+00, -2.22273e+00, -2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
        [0,1,0,0,0, 2.61016e+00, -2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -3.27246e+00, -3.12435e+00, -2.11824e+00],
        [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
        [1,0,0,0,0, -3.07348e+00, -2.51692e+00, -1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.]]

# pred = torch.tensor(a)
# true = torch.tensor(at)

# nb = pred.shape[0]
# nb_num = int(nb * (nb - 1) / 2)
#
# rd_d = torch.zeros(nb_num)
# rd_d_ps = pred.clone()  # [imgs, 14]
# # rd_d_ps = rd_d_ps.sigmoid().cpu()       # sigmoid预处理
# rd_d_ps = rd_d_ps.cpu()  # [imgs, 14]
#
# rd_t = torch.zeros(nb_num)
# rd_d_t = true.detach()
# rd_d_t = rd_d_t.cpu()
#
# sign_rd_correspond = torch.zeros(nb_num)  # [nb_num]
# sign_rd_sum_correspond = torch.zeros(nb_num)  # [nb_num]
# rd_d_psng = pred.detach().cpu()  # no grad    [imgs, 14]


'''feature weights —— estimate feature in same quandrant or not'''
# sign_rd = (torch.tanh(rd_d_psng[:, 5:] * 10000) + 1) / 2  # [imgs, 9]
# print("\n sign_rd \n", sign_rd)


'''***************************** start *****************************'''
# t_d_s = time.time()
# sign_rd_sum = torch.sum(sign_rd, 1)  # [imgs]
# # print("\n sign_rd_sum \n", sign_rd_sum)
# sign_n = 0
# for sign_di in range(nb):
#         for sign_dj in range(sign_di + 1, nb):
#                 # the same quandrant or not: same quandrant = 1.0 must differ
#                 sign_rd_correspond[sign_n] = 1.0 if torch.equal(sign_rd[sign_di],
#                                                                 sign_rd[sign_dj]) else 10.0  # [nb_num]
#                 if torch.equal(sign_rd_sum[sign_di], sign_rd_sum[sign_dj]):
#                         sign_rd_sum_correspond[sign_n] = 0.0  # [nb_num]
#                 else:
#                         # the adjoin quandrant or not: adjoin quandrant = -5.0 may similar
#                         sign_rd_sum_correspond[sign_n] = -5.0 if torch.sub(sign_rd[sign_di], sign_rd[
#                                 sign_dj]).abs().sum() == 1 else 0.0  # [nb_num]
#                 sign_n += 1
# sign_rd_weight = sign_rd_correspond + sign_rd_sum_correspond  # [nb_num]
# t_d_e = time.time() - t_d_s
# print("\n t_d中计算度量用时 ===> ", t_d_e)
# print("\n sign_rd_weight \n", sign_rd_weight)
# '''********************* 加速操作 *********************'''
# t_n_s = time.time()
# ones = torch.ones_like(sign_rd_correspond)
# tens = ones * 10
# others = ones * 5
# r1_correspond_num = torch.pdist(sign_rd, p=1)
# print("\n r1_correspond_num \n", r1_correspond_num)
# r1_correspond_num = torch.where(r1_correspond_num > 1.0, tens, r1_correspond_num)
# r1_correspond_num = torch.where(r1_correspond_num == 1.0, others, r1_correspond_num)
# r1_correspond_num = torch.where(r1_correspond_num == 0.0, ones, r1_correspond_num)
# t_n_e = time.time() - t_n_s
# print("\n t_n_d中计算度量用时 ===> %16f" % (t_n_e))
# print("\n r1_correspond_num \n", r1_correspond_num)
'''***************************** end *****************************'''


'''element of feature weights —— magnify the max element'''
# temp_a = torch.argmax(rd_d_ps[:, 5:], dim=1)  # [imgs]
# # print("\n temp_a \n", temp_a)
#
# rd_weight = torch.ones_like(rd_d_ps[:, 5:])  # [imgs, 9]
# temp_b = torch.arange(temp_a.shape[0])  # [imgs]
# rd_weight = rd_weight.index_put([temp_b, temp_a], values=torch.tensor(10.0))  # [imgs, 9]
# # print("\n rd_weight \n", rd_weight)
#
# rd_d_ps[:, 5:] = torch.mul(rd_d_ps[:, 5:], rd_weight)  # to magnify the max element
# # print("\n rd_d_ps \n", rd_d_ps)
#
# sign_rd_tp = torch.tanh(rd_d_psng[:, 5:] * 10000)  # [imgs, 9]
# # print("\n sign_rd_tp \n", sign_rd_tp)
#
# rd_d_ps[:, 5:] = torch.mul(rd_d_ps[:, 5:], sign_rd_tp)  # to positive
# # print("\n rd_d_ps \n", rd_d_ps)


'''=========================================== 厉害的模块 ========================================='''
# t_d_s = time.time()
# dddd = torch.nn.functional.pdist(rd_d_ps[:, 5:], p=1)
# t_d_e = time.time() - t_d_s
# print("\n t_d中计算度量用时 ===> ", t_d_e)
# print("输出tensor中两两比较的结果上三角 dddd ===>", dddd)

# print("rd_d_ps[:, 5:, None] ===>", rd_d_ps[:, None, 5:])
# dddd_norm = torch.norm(rd_d_ps[:, None, 5:] - rd_d_ps[:, 5:], dim=2, p=1)
# print("输出tensor中两两比较的结果矩阵 dddd_norm ===>", dddd_norm)

# t_t_s = time.time()
# tttt = torch.nn.functional.pdist(rd_d_t, p=1)
# t_t_e = time.time() - t_t_s
# print("\n t_t中计算度量用时 ===> ", t_t_e)
# print("tttt ===>", tttt)
#
# print("rd_d_t[:, None] ===>", rd_d_t[:, None])
# tttt_norm = torch.norm(rd_d_t[:, None] - rd_d_t, dim=2, p=1)
# print("tttt_norm ===>", tttt_norm)


'''distance betown two feature in N.1 quandrant(all positive)'''
'''***************************** start *****************************'''
# t_rd_s = time.time()
# rd_n = 0
# for rd_di in range(nb):
#         for rd_dj in range(rd_di + 1, nb):
#                 rd_d[rd_n] = torch.sub(rd_d_ps[rd_di, 5:], rd_d_ps[rd_dj, 5:]).abs().sum()  # [nb_num]
#                 rd_t[rd_n] = 0.0 if torch.equal(rd_d_t[rd_di], rd_d_t[rd_dj]) else 1.0  # [nb_num]
#                 rd_n += 1
# t_rd_e = time.time() - t_rd_s
# print("\n t_rd中计算度量用时 ===> ", t_rd_e)
# print("\n rd_d \n", rd_d)
# print("\n rd_t \n", rd_t)
# '''********************* 加速操作 *********************'''
# t_rdn_s = time.time()
# ones = torch.ones_like(rd_d)
# tens = ones * 10
# others = ones * 5
# r1_d = torch.pdist(rd_d_ps[:, 5:], p=1)
# r1_t = torch.pdist(rd_d_t, p=1)
# r1_t = torch.where(r1_t > 0.0, ones, r1_t)
# t_rdn_e = time.time() - t_rdn_s
# print("\n t_rdn中计算度量用时 ===> ", t_rdn_e)
# print("\n rd_d_n \n", r1_d)
# print("\n rd_t_n \n", r1_t)
'''***************************** end *****************************'''


# rd_d = rd_d * sign_rd_weight  # [nb_num]
# # print("\n rd_d 加权后\n", rd_d)
#
# rd_pre = rd_d.to(pred.device)
# # rd_target = rd_t.to(true.device)
#
# pred_min = torch.min(rd_pre)  # [1]
# pred_prob = rd_pre + 0.005  # [nb_num]
# pred_max = torch.max(pred_prob)  # [1]
# '''************** 无sigmoid **************'''  # 1
# # pred_prob = pred_prob / (pred_max + 0.005)
# '''************** 有sigmoid **************'''  # 3  sigmoid 后处理
# pred_prob = pred_prob / (pred_max + 0.005)  # [nb_num]
# # print("\n pred_prob \n", pred_prob)
# pred_prob = 10 * (pred_prob - 0.5)  # [nb_num]
# pred_prob = torch.sigmoid(pred_prob)  # [nb_num]
# # print("\n pred_prob sigmoid处理后 \n", pred_prob)





ps = torch.tensor(a)
t = torch.tensor(at)
nb = ps.shape[0]

rd_d = torch.zeros([nb, nb])
rd_d_ps = ps.clone()
# rd_d_ps = rd_d_ps.sigmoid().cpu()  # sigmoid预处理
rd_d_ps = rd_d_ps.cpu()

rd_t = torch.zeros([nb, nb])
rd_d_t = t.detach()
rd_d_t = rd_d_t.cpu()

sign_rd_correspond = torch.zeros([nb, nb])
sign_rd_sum_correspond = torch.zeros([nb, nb])
rd_d_psng = ps.detach().cpu()  # no grad

'''feature weights —— estimate feature in same quandrant or not'''
sign_rd = (torch.tanh(rd_d_psng[:, 5:] * 10000) + 1) / 2


'''***************************** start *****************************'''
t_d_s = time.time()
sign_rd_sum = torch.sum(sign_rd, 1)
for sign_di in range(nb):
        for sign_dj in range(sign_di + 1, nb):
            # the same quandrant or not: same quandrant = 1.0 must differ
            sign_rd_correspond[sign_di, sign_dj] = 1.0 if torch.equal(sign_rd[sign_di], sign_rd[sign_dj]) else 10.0
            if torch.equal(sign_rd_sum[sign_di], sign_rd_sum[sign_dj]):
                sign_rd_sum_correspond[sign_di, sign_dj] = 0.0
            else:
                # the adjoin quandrant or not: adjoin quandrant = -5.0 may similar
                sign_rd_sum_correspond[sign_di, sign_dj] = -5.0 if torch.sub(sign_rd[sign_di], sign_rd[sign_dj]).abs().sum() == 1 else 0.0
            sign_rd_correspond[sign_dj, sign_di] = sign_rd_correspond[sign_di, sign_dj]
            sign_rd_sum_correspond[sign_dj, sign_di] = sign_rd_sum_correspond[sign_di, sign_dj]
sign_rd_weight = sign_rd_correspond + sign_rd_sum_correspond
t_d_e = time.time() - t_d_s
print("\n t_d中计算度量用时 ===> ", t_d_e)
print("\n sign_rd_correspond \n", sign_rd_correspond)
print("\n sign_rd_sum_correspond \n", sign_rd_sum_correspond)
print("\n sign_rd_weight \n", sign_rd_weight)
'''********************* 加速操作 *********************'''
t_n_s = time.time()
ones = torch.ones_like(sign_rd_correspond)
tens = ones * 10
others = ones * 5
r1_correspond_num = torch.norm(sign_rd[:, None] - sign_rd, dim=2, p=1)
print("\n r1_correspond_num \n", r1_correspond_num)
r1_correspond_num = torch.where(r1_correspond_num > 1.0, tens, r1_correspond_num)
r1_correspond_num = torch.where(r1_correspond_num == 1.0, others, r1_correspond_num)
r1_correspond_num = torch.where(r1_correspond_num == 0.0, ones, r1_correspond_num)
t_n_e = time.time() - t_n_s
print("\n t_n_d中计算度量用时 ===> %16f" % (t_n_e))
print("\n r1_correspond_num \n", r1_correspond_num)
'''***************************** end *****************************'''


'''element of feature weights —— magnify the max element'''
temp_a = torch.argmax(rd_d_ps[:, 5:], dim=1)
rd_weight = torch.ones_like(rd_d_ps[:, 5:])
temp_b = torch.arange(temp_a.shape[0])
rd_weight = rd_weight.index_put([temp_b, temp_a], values=torch.tensor(10.0))
print("\n rd_weight \n", rd_weight)

rd_d_ps[:, 5:] = torch.mul(rd_d_ps[:, 5:], rd_weight)
sign_rd_tp = torch.tanh(rd_d_psng[:, 5:] * 10000)
rd_d_ps[:, 5:] = torch.mul(rd_d_ps[:, 5:], sign_rd_tp)  # to positive
print("\n rd_d_ps \n", rd_d_ps)


'''***************************** start *****************************'''
t_rd_s = time.time()
for rd_di in range(nb):
        for rd_dj in range(rd_di + 1, nb):
            rd_d[rd_di, rd_dj] = torch.sub(rd_d_ps[rd_di, 5:], rd_d_ps[rd_dj, 5:]).abs().sum()
            rd_d[rd_dj, rd_di] = rd_d[rd_di, rd_dj]
            rd_t[rd_di, rd_dj] = -1.0 if torch.equal(rd_d_t[rd_di], rd_d_t[rd_dj]) else 1.0
            rd_t[rd_dj, rd_di] = rd_t[rd_di, rd_dj]
            '''********** wst的评价模块->check wst-w **********'''  # 4
            # if torch.equal(rd_d_t[rd_di], rd_d_t[rd_dj]):
            #     wst_check_0[tcls[i][rd_di], tcls[i][rd_dj]] += 1
            # else:
            #     wst_check_1[tcls[i][rd_di], tcls[i][rd_dj]] += 1
t_rd_e = time.time() - t_rd_s
print("\n t_rd中计算度量用时 ===> ", t_rd_e)
print("\n rd_d \n", rd_d)
print("\n rd_t \n", rd_t)
'''********************* 加速操作 *********************'''
t_rdn_s = time.time()
ones = torch.ones_like(rd_d)
tens = ones * -1
others = ones * 5
r1_d_n = torch.norm(rd_d_ps[:, None, 5:] - rd_d_ps[:, 5:], dim=2, p=1)
# r1_t_n = torch.norm(rd_d_t[:, None] - rd_d_t, dim=2, p=1)
# r1_t_n = torch.where(r1_t_n > 0.0, ones, r1_t_n)
t_rdn_e = time.time() - t_rdn_s
print("\n t_rdn中计算度量用时 ===> ", t_rdn_e)
print("\n r1_d_n \n", r1_d_n)
# print("\n r1_t_n \n", r1_t_n)
'''***************************** end *****************************'''


rd_d = rd_d * sign_rd_weight
print("\n rd_d 加权后 \n", rd_d)

r1_d_n = r1_d_n * r1_correspond_num
print("\n r1_d_n 加权后 \n", r1_d_n)


wst_pre = rd_d.to(ps.device)
wst_target = rd_t.to(t.device)

pred_min = torch.min(wst_pre)
pred_prob = wst_pre + 0.001
pred_max = torch.max(pred_prob)
'''************** 无sigmoid **************'''  # 1
# pred_prob = pred_prob / (pred_max + 0.002)
'''************** 有sigmoid **************'''  # 3
pred_prob = pred_prob / (pred_max + 0.002)
pred_prob = 10 * (pred_prob - 0.5)
pred_prob = torch.sigmoid(pred_prob)



# tensor([[0.00000, 0.48283, 0.41853, 0.26665, 0.43492, 0.39422, 0.68343, 0.37975, 0.38167, 0.60130],
#         [0.48283, 0.00000, 0.52829, 0.37785, 0.32788, 0.80114, 0.52027, 0.68981, 0.77817, 0.87026],
#         [0.41853, 0.52829, 0.00000, 0.16904, 0.46682, 0.71377, 0.54718, 0.25761, 0.61672, 0.78948],
#         [0.26665, 0.37785, 0.16904, 0.00000, 0.38093, 0.62690, 0.59891, 0.34064, 0.53479, 0.71674],
#         [0.43492, 0.32788, 0.46682, 0.38093, 0.00000, 0.54245, 0.48188, 0.51332, 0.57014, 0.56605],
#         [0.39422, 0.80114, 0.71377, 0.62690, 0.54245, 0.00000, 0.57521, 0.50052, 0.45772, 0.24566],
#         [0.68343, 0.52027, 0.54718, 0.59891, 0.48188, 0.57521, 0.00000, 0.54842, 0.87929, 0.64443],
#         [0.37975, 0.68981, 0.25761, 0.34064, 0.51332, 0.50052, 0.54842, 0.00000, 0.48286, 0.57418],
#         [0.38167, 0.77817, 0.61672, 0.53479, 0.57014, 0.45772, 0.87929, 0.48286, 0.00000, 0.68065],
#         [0.60130, 0.87026, 0.78948, 0.71674, 0.56605, 0.24566, 0.64443, 0.57418, 0.68065, 0.00000]])

