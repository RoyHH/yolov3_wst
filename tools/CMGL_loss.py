import torch
import torch.nn as nn
import numpy as np
import cv2
import time
import matplotlib.pyplot as plt


# a = [[0,0,0,0,0, 0.09850, 0.07381, 0.09498, 0.06709, 0.04420, 0.09838, -0.07472, 0.09704, 0.10511],
#         [0,0,0,0,0, 0.12517, 0.08320, 0.05671, 0.08169, 0.03311, 0.11040, -0.07720, 0.10210, 0.09071],
#         [0,0,0,0,0, 0.08007, 0.08626, 0.07167, 0.08454, 0.03979, 0.07875, -0.10219, 0.07290, 0.08353],
#         [0,0,0,0,0, 0.04756, 0.09166, 0.06956, 0.15907, 0.09441, 0.09501, -0.06844, 0.05258, 0.05222],
#         [0,0,0,0,0, 0.04652, 0.06091, 0.04461, 0.16689, 0.05162, 0.07181, -0.07676, 0.04061, 0.05499],
#         [0,0,0,0,0, 0.05683, 0.05448, 0.02966, 0.15260, 0.12075, 0.06657, -0.06459, 0.04622, 0.05860],
#         [0,0,0,0,0, 0.06292, 0.09569, 0.04262, 0.06236, 0.07043, 0.06593, -0.02867, 0.10360, 0.08807],
#         [0,0,0,0,0, 0.16726, 0.07523, 0.04722, 0.05191, 0.04980, 0.06777, -0.05449, 0.04418, 0.07774]]

a = [[0,0,0,0,0, -5.90986, -13.04762,   6.41996,  -6.02352, -10.64276, -15.09558,  9.41367, -16.17023, -12.35209],
        [0,0,0,0,0, -7.21984,   4.77345, -17.08892, -19.98676,  -6.65397, -10.56651, -15.72788,  -4.23451, -11.30104],
        [0,0,0,0,0, 7.56419,  -5.15149,  -5.77484, -12.28424, -10.24066,  -8.07071,  -8.84589,  -9.55158, -11.28349],
        [0,0,0,0,0, -4.41505, -11.84778,   5.30759,  -4.94095,  -8.08970, -14.26806,  -8.22902, -14.45283, -11.11575],
        [0,0,0,0,0, -4.85881, -12.93312,   6.13393,  -4.75256,  -8.30600, -15.60899,  9.30687, -15.94003, -11.89202],
        [0,0,0,0,0, -9.16481,   4.85175, -18.92580, -18.22492, -10.74850,  -7.82335, -12.80189,  -2.70451, -12.57616],
        [0,1,0,0,0, -2.91972e+00, -2.20095e+00, 3.48893e+00, -2.59476e+00, -2.79146e+00, -2.14687e+00, -1.84426e+00, -2.32959e+00, -2.42477e+00],
        [0,0,1,0,0, 2.26935e+00, -2.67812e+00, -3.04553e+00, -3.46773e+00, -2.81742e+00, -2.19005e+00, -2.42943e+00, -2.88006e+00, -2.69119e+00],
        [0,0,0,1,0, -2.82522e+00, -2.65070e+00, -3.47379e+00, -1.19072e+00, -2.82531e+00, -2.53264e+00, -2.48552e+00, -3.06835e+00, -2.50541e+00],
        [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
        [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
        [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
        [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
        [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00],
        [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
        [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 1., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [1., 0., 0., 0., 0., 0., 0., 0., 0.],
        [0., 1., 0., 0., 0., 0., 0., 0., 0.],
        [0., 0., 0., 1., 0., 0., 0., 0., 0.],
        [0., 0., 0., 0., 0., 0., 1., 0., 0.],
        [0., 0., 0., 0., 0., 0., 1., 0., 0.]]

# a =[[0,1,0,0,0, -2.91972e+00, -2.20095e+00, 3.48893e+00, -2.59476e+00, -2.79146e+00, -2.14687e+00, -1.84426e+00, -2.32959e+00, -2.42477e+00],
#         [0,0,1,0,0, 2.26935e+00, -2.67812e+00, -3.04553e+00, -3.46773e+00, -2.81742e+00, -2.19005e+00, -2.42943e+00, -2.88006e+00, -2.69119e+00],
#         [0,0,0,1,0, -2.82522e+00, -2.65070e+00, -3.47379e+00, -1.19072e+00, -2.82531e+00, -2.53264e+00, -2.48552e+00, -3.06835e+00, -2.50541e+00],
#         [0,0,0,1,0, -2.39842e+00, 2.75852e+00, -2.76431e+00, -2.82616e+00, 1.85408e+00, -1.95794e+00, -2.06213e+00, -3.06259e+00, -2.48557e+00],
#         [0,1,0,0,0, -2.36682e+00, -2.49370e+00, 2.30359e+00, -2.47418e+00, 2.11215e+00, -1.73196e+00, -1.86886e+00, -2.87890e+00, -1.85472e+00],
#         [0,0,1,0,0, -2.76417e+00, 2.48319e+00, -2.71835e+00, -2.89563e+00, 2.46679e+00, 2.52908e+00, -1.83423e+00, -3.05829e+00, -2.87755e+00],
#         [0,0,0,0,1, -2.36243e+00, -2.22273e+00, 2.54640e+00, -2.50266e+00, -2.10372e+00, -1.97586e+00, -2.11248e+00, -3.19947e+00, -1.95671e+00],
#         [0,1,0,0,0, -2.61016e+00, 2.55572e+00, -2.37749e+00, -3.10570e+00, -2.51313e+00, -2.30006e+00, -2.27246e+00, -3.12435e+00, -2.11824e+00],
#         [0,0,0,1,0, -2.30145e+00, -2.48564e+00, -2.01462e+00, -1.99276e+00, -1.92442e+00, 2.76685e+00, -3.22923e+00, -3.31306e+00, -2.96676e+00],
#         [1,0,0,0,0, -3.07348e+00, -2.51692e+00, 1.77885e+00, -2.35907e+00, -2.39900e+00, -2.71800e+00, -2.42641e+00, 3.20143e+00, -3.23324e+00]]

# at = [[0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [1., 0., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 1., 0., 0., 0., 0., 0.],
#         [0., 0., 1., 0., 0., 0., 0., 0., 0.],
#         [0., 1., 0., 0., 0., 0., 0., 0., 0.],
#         [1., 0., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 1., 0., 0., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 1., 0., 0., 0., 0., 0.],
#         [0., 0., 0., 0., 0., 0., 1., 0., 0.]]


'''############################ 详细版 ############################'''
# ps = torch.tensor(a, requires_grad=True)
# t = torch.tensor(at)
# nb = ps.shape[0]
#
#
# # k_t = torch.norm(t[:, None]-t, dim=2, p=1)
# # # print("k_t", k_t)
#
#
# '''====================== ps中的feature vecs做预处理 ======================'''
# _, k_t_id = torch.max(t, dim=1)      # find the class of every feature vec
# print("k_t_id \n", k_t_id[:, None])
# new_ps = ps[:, 5:].clone()
# print("\n new_ps \n", new_ps)
# new_ps = torch.cat((k_t_id[:, None], new_ps), 1)      # feature vec里在第一个元素上加入类别标签
# print("\n new_ps with class label in the first one of row \n", new_ps)
#
#
# '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
# '''
# idx_ci：当前ps序列中属于i类的行索引
# ps_ci：当前ps序列中属于i类的所以feature vecs的集合
# m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
# '''
# new_ps_n = new_ps.detach().numpy()      # 便于操作
#
# names = locals()      # 便于动态命名，使其变量名能与类别标签对应上
# for ci in range(9):
#         names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
#         # print("idx_c%s = \n %s \n" % (ci, names['idx_c%s' % ci]))
#
#         if names['idx_c%s' % ci].size != 0:
#                 names['ps_c%s' % ci] = new_ps[torch.from_numpy(names['idx_c%s' % ci]), 1:]
#                 names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
#                 # print("ps_c%s = \n %s \n" % (ci, names['ps_c%s' % ci]))
#
#                 names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
#                 # print("m_ps_c%s = \n %s \n" % (ci, names['m_ps_c%s' % ci]))
#
#                 '''画图，便于观察'''
#                 if names['ps_c%s' % ci].shape[0] != 0:
#                         plt.figure()
#                         x = np.arange(0, 9)
#
#                         ps_ci_np = names['ps_c%s' % ci].detach().numpy()
#                         m_ps_ci_np = names['m_ps_c%s' % ci].detach().numpy()
#                         # print("ps_ci_np", ps_ci_np[0, :])
#                         # print("x", x)
#                         for i in range(ps_ci_np.shape[0]):
#                             plt.plot(x, ps_ci_np[i, :], color='salmon', linestyle='--')
#                         plt.plot(x, m_ps_ci_np, color='red', linewidth=3, label='means')
#                         plt.xlabel("features_vec")
#                         plt.ylabel("data")
#                         plt.title("mean")
#                         plt.show()
#
#
# '''ct：统计当前ps序列中所有出现的类别'''
# ct_idx = t.sum(dim=0)
# print('\n ct_idx \n', ct_idx)
# ct_idx = ct_idx.numpy()
# ct = np.argwhere(ct_idx != 0)
# print('\n ct \n', ct)
# ct = torch.from_numpy(ct)
#
#
# '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
#         else:
#                 m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
# print('\n m_ps_cs \n', m_ps_cs)
#
#
# '''====================== 计算差值 ======================'''
# ''' rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1） '''
# rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
# print('\n rd_all \n', rd_all)     # [ps.shape, Num(fv-m)]
#
#
# '''
# rd_ci：ps中属于i类别的feature vecs与i类别的均值的差值（L1）
# rd_ci_sum：统计所有rd_ci的和
# '''
# # rd_ct_idx = torch.zeros(9)
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
#         # print('rd_c%s = \n %s \n' % (temp_ci, names['rd_c%s' % temp_ci]))
#         # rd_ct_idx[temp_ci] = 1
#         # if ci == 0:
#         #         rd_ci_sum = names['rd_c%s' % temp_ci].sum()
#         # else:
#         #         rd_ci_sum = rd_ci_sum + names['rd_c%s' % temp_ci].sum()
# # print('\n rd_ci_sum \n', rd_ci_sum)
# # print('\n rd_ct_idx \n', rd_ct_idx)
# # print('\n rd_all.sum() \n', rd_all.sum(dim=1))
#
#
# '''********** v1 不区分类别**********'''
# '''
# rd_sum_d1：ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）的和
# rd_d_sum：ps的rd差值（L1）和
# rd_d_sum_p：ps的rd差值（L1）和的单位化
# '''
# # rd_sum_d1 = rd_all.sum() - m_ps_cs.shape[0] * rd_ci_sum
# # print('\n rd_sum_d1 \n', rd_sum_d1)
# # rd_d_sum = 0.5 * torch.div(rd_sum_d1, (m_ps_cs.shape[0] - 1)) + 0.5 * rd_ci_sum
# # print('\n rd_d_sum \n', rd_d_sum)
# # rd_d_sum_p = torch.div(rd_d_sum, ps.shape[0])
# # print('\n rd_d_sum_p \n', rd_d_sum_p)
#
#
# '''********** v2 **********'''
# '''
# rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
# idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
# '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 rd_cs_ps = names['rd_c%s' % temp_ci]
#                 temp_idx = torch.from_numpy(names['idx_c%s' % temp_ci]).t().squeeze(dim=0)
#                 # print("idx_c%s = \n %s \n" % (temp_ci, names['idx_c%s' % temp_ci]))
#                 idx_ct_ps = temp_idx
#         else:
#                 rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
#                 # print("rd_c%s = \n %s \n" % (ct[ci, 0], names['rd_c%s' % temp_ci]))
#                 temp_idx = torch.from_numpy(names['idx_c%s' % temp_ci]).t().squeeze(dim=0)
#                 # print("idx_c%s = \n %s \n" % (temp_ci, names['idx_c%s' % temp_ci]))
#                 idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)
# # print('\n rd_cs_ps \n', rd_cs_ps)
#
#
# '''
# rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
# rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
# '''
# idx_ct_ps = idx_ct_ps.to(torch.int64)
# print('\n idx_ct_ps \n', idx_ct_ps)
# rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
# rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)
# print('\n rd_all_ct_ps \n', rd_all_ct_ps)
# print('\n rt_all_ct_ps \n', rt_all_ct_ps)
#
#
# '''
# rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
# rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
# '''
# rd_cs_ps_d0 = rd_cs_ps[None, :].t()
# print('\n rd_cs_ps_d0 \n', rd_cs_ps_d0)
# temp_0_min = torch.min(rd_cs_ps_d0)
# temp_0_max = torch.max(rd_cs_ps_d0)
# # print("temp_0_max ==", temp_0_max)
# # print("temp_0_min ==", temp_0_min)
# # rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0).abs()
# rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)
# print('\n rd_cs_ps_d1 \n', rd_cs_ps_d1)
#
#
# ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 ci_idx = ci
#         else:
#                 ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
#         # print('\n ci_idx \n', int(ci_idx))
#         # print('\n ct_idx[ci] \n', int(ct_idx[temp_ci]))
#         names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#         # print("\n rd_cs_ps_d1_c%s = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s' % temp_ci]))
#         names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#         # print("\n rd_cs_ps_d0_c%s = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s' % temp_ci]))
#
#         # print('size(1)', names['rd_cs_ps_d1_c%s' % temp_ci].size(1))
#         # print("ci", ci)
#         names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
#         # print("rd_cs_ps_d1_c%s = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s' % temp_ci]))
#
#         if ci == 0:
#                 temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
#                 temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
#
#         else:
#                 temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
#                 temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min
#         # print("temp_1_max ==", temp_1_max)
#         # print("temp_1_min ==", temp_1_min)
#
#
# ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
#                 # print("\n rd_cs_ps_d1_c%s = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s' % temp_ci]))
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
#                 # print("rd_cs_ps_d1_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s_p' % temp_ci]))
#
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci], (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#                 # print("rd_cs_ps_d1_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s_p' % temp_ci]))
#
#                 # names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + 0.5)
#                 temp_1_mid = torch.div(20, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)
#                 # print("rd_cs_ps_d1_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s_p' % temp_ci]))
#
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])
#                 print("rd_cs_ps_d1_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s_p' % temp_ci]))
#
#
#                 # print("\n rd_cs_ps_d0_c%s = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s' % temp_ci]))
#                 # names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] - temp_0_min + 0.001
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] + 0.001
#                 # print("rd_cs_ps_d0_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s_p' % temp_ci]))
#
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci], (0.002 + (temp_0_max + 0.001)))
#                 # print("rd_cs_ps_d0_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s_p' % temp_ci]))
#
#                 # names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - 0.5)
#                 temp_0_mid = torch.div(10, (0.002 + (temp_0_max + 0.001)))
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - temp_0_mid)
#                 # print("rd_cs_ps_d0_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s_p' % temp_ci]))
#
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])
#                 print("rd_cs_ps_d0_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s_p' % temp_ci]))



'''############################ 简化版 ############################'''
# ps = torch.tensor(a, requires_grad=True)
# t = torch.tensor(at)
# nb = ps.shape[0]
#
#
# '''====================== ps中的feature vecs做预处理 ======================'''
# _, k_t_id = torch.max(t, dim=1)      # find the class of every feature vec
# new_ps = ps[:, 5:].clone()
# new_ps = torch.cat((k_t_id[:, None], new_ps), 1)      # feature vec里在第一个元素上加入类别标签
#
#
# '''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
# '''
# idx_ci：当前ps序列中属于i类的行索引
# ps_ci：当前ps序列中属于i类的所以feature vecs的集合
# m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
# '''
# new_ps_n = new_ps.detach().numpy()      # 便于操作
#
# names = locals()      # 便于动态命名，使其变量名能与类别标签对应上
# for ci in range(9):
#         names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
#         if names['idx_c%s' % ci].size != 0:
#                 names['ps_c%s' % ci] = new_ps[torch.from_numpy(names['idx_c%s' % ci]), 1:]
#                 names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
#
#                 names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
#
#                 '''画图，便于观察'''
#                 # if names['ps_c%s' % ci].shape[0] != 0:
#                 #         plt.figure()
#                 #         x = np.arange(0, 9)
#                 #         ps_ci_np = names['ps_c%s' % ci].detach().numpy()
#                 #         m_ps_ci_np = names['m_ps_c%s' % ci].detach().numpy()
#                 #         # print("ps_ci_np", ps_ci_np[0, :])
#                 #         # print("x", x)
#                 #         for i in range(ps_ci_np.shape[0]):
#                 #             plt.plot(x, ps_ci_np[i, :], color='salmon', linestyle='--')
#                 #         plt.plot(x, m_ps_ci_np, color='red', linewidth=3, label='means')
#                 #         plt.xlabel("features_vec")
#                 #         plt.ylabel("data")
#                 #         plt.title("mean")
#                 #         plt.show()
#
# '''ct：统计当前ps序列中所有出现的类别'''
# ct_idx = t.sum(dim=0)
# print('\n ct_idx \n', ct_idx)
# ct_idx = ct_idx.numpy()
# ct = np.argwhere(ct_idx != 0)
# print('\n ct \n', ct)
# ct = torch.from_numpy(ct)
#
# '''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
#         else:
#                 m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)
# print('\n m_ps_cs \n', m_ps_cs)
#
#
# '''====================== 计算差值 ======================'''
# ''' rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1） '''
# rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)
# print('\n rd_all \n', rd_all)     # [ps.shape, Num(fv-m)]
#
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)
#
# '''
# rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
# idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
# '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 rd_cs_ps = names['rd_c%s' % temp_ci]
#                 temp_idx = torch.from_numpy(names['idx_c%s' % temp_ci]).t().squeeze(dim=0)
#                 idx_ct_ps = temp_idx
#         else:
#                 rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
#                 temp_idx = torch.from_numpy(names['idx_c%s' % temp_ci]).t().squeeze(dim=0)
#                 idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)
#
# '''
# rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组
# rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组
# '''
# idx_ct_ps = idx_ct_ps.to(torch.int64)
# rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
# rt_all_ct_ps = torch.index_select(t, 0, idx_ct_ps)
#
# '''
# rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1）
# rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1）
# '''
# rd_cs_ps_d0 = rd_cs_ps[None, :].t()
# print('\n rd_cs_ps_d0 \n', rd_cs_ps_d0)
# temp_0_min = torch.min(rd_cs_ps_d0)
# temp_0_max = torch.max(rd_cs_ps_d0)
# rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)
# print('\n rd_cs_ps_d1 \n', rd_cs_ps_d1)
#
# ''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if ci == 0:
#                 ci_idx = ci
#         else:
#                 ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
#         names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#         names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
#         names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]
#
#         if ci == 0:
#                 temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
#                 temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
#         else:
#                 temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
#                 temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min
#
# ''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
# for ci in range(ct.shape[0]):
#         temp_ci = int(ct[ci, 0])
#         if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci], (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#
#                 temp_1_mid = torch.div(20, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)
#
#                 names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])
#                 print("rd_cs_ps_d1_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d1_c%s_p' % temp_ci]))
#
#
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] + 0.001
#
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci], (0.002 + (temp_0_max + 0.001)))
#
#                 temp_0_mid = torch.div(10, (0.002 + (temp_0_max + 0.001)))
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - temp_0_mid)
#
#                 names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])
#                 print("rd_cs_ps_d0_c%s_p = \n %s \n" % (temp_ci, names['rd_cs_ps_d0_c%s_p' % temp_ci]))




'''############################ 精简化版 ############################'''
pred = torch.tensor(a, requires_grad=True)
true = torch.tensor(at)
nb = pred.shape[0]
nc = 9

'''====================== ps中的feature vecs做预处理 ======================'''
_, k_t_id = torch.max(true, dim=1)      # find the class of every feature vec
ps = pred.clone()
new_ps = pred[:, 5:].clone()
new_ps = torch.cat((k_t_id[:, None], new_ps), 1)      # feature vec里在第一个元素上加入类别标签


'''====================== 基于类别，统计各类feature vecs的均值向量 ======================'''
'''ct：统计当前ps序列中所有出现的类别'''
ct_idx = true.sum(dim=0)
ct_idx = ct_idx.cpu().numpy()
ct = np.argwhere(ct_idx != 0)
ct = torch.from_numpy(ct)
ct = ct.to(new_ps.device)

'''
idx_ci：当前ps序列中属于i类的行索引
ps_ci：当前ps序列中属于i类的所以feature vecs的集合
m_ps_ci：当前ps序列中属于i类的所以feature vecs的均值向量
'''
new_ps_n = new_ps.detach().numpy()      # 便于操作

names = locals()      # 便于动态命名，使其变量名能与类别标签对应上
for ci in range(nc):
        names['idx_c%s' % ci] = np.argwhere(new_ps_n[:, 0] == ci)
        if names['idx_c%s' % ci].size != 0:
                names['idx_c%s' % ci] = torch.from_numpy(names['idx_c%s' % ci]).to(new_ps.device)
                names['ps_c%s' % ci] = new_ps[names['idx_c%s' % ci], 1:]
                names['ps_c%s' % ci] = names['ps_c%s' % ci].squeeze(1)
                names['m_ps_c%s' % ci] = torch.mean(names['ps_c%s' % ci], dim=0)
                '''画图，便于观察'''
                # if names['ps_c%s' % ci].shape[0] != 0:
                #         plt.figure()
                #         x = np.arange(0, nc)
                #         ps_ci_np = names['ps_c%s' % ci].detach().numpy()
                #         m_ps_ci_np = names['m_ps_c%s' % ci].detach().numpy()
                #         # print("ps_ci_np", ps_ci_np[0, :])
                #         # print("x", x)
                #         for i in range(ps_ci_np.shape[0]):
                #             plt.plot(x, ps_ci_np[i, :], color='salmon', linestyle='--')
                #         plt.plot(x, m_ps_ci_np, color='red', linewidth=3, label='means')
                #         plt.xlabel("features_vec")
                #         plt.ylabel("data")
                #         plt.title("mean")
                #         plt.show()


'''m_ps_cs：当前ps序列中所有出现的类别的均值向量集合'''
for ci in range(ct.shape[0]):
        temp_ci = int(ct[ci, 0])
        if ci == 0:
                m_ps_cs = names['m_ps_c%s' % temp_ci].unsqueeze(0)
        else:
                m_ps_cs = torch.cat((m_ps_cs, names['m_ps_c%s' % temp_ci].unsqueeze(0)), 0)


'''====================== 计算差值 ======================'''
''' 
rd_cs_ps：将所有rd_ci按照类别号依次拼接为ps.shape[0]大小的集合
idx_ct_ps：rd_cs_ps 中元素对应的feature vecs在ps中的行索引
'''
for ci in range(ct.shape[0]):
        temp_ci = int(ct[ci, 0])
        names['rd_c%s' % temp_ci] = torch.sub(names['ps_c%s' % temp_ci], names['m_ps_c%s' % temp_ci]).abs().sum(dim=1)

        if ci == 0:
                rd_cs_ps = names['rd_c%s' % temp_ci]
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = temp_idx
        else:
                rd_cs_ps = torch.cat((rd_cs_ps, names['rd_c%s' % temp_ci]), 0)
                temp_idx = names['idx_c%s' % temp_ci].t().squeeze(dim=0)
                idx_ct_ps = torch.cat((idx_ct_ps, temp_idx), 0)

''' 
rd_all：ps中所有feature vecs与ps中所有出现过的类别的均值的差值（L1） 
'''
rd_all = torch.norm(ps[:, None, 5:] - m_ps_cs, dim=2, p=1)

''' 
rd_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将ps重组 
rt_all_ct_ps：按照 rd_cs_ps 中元素对应的feature vecs的序列将t重组 
'''
idx_ct_ps = idx_ct_ps.to(torch.int64)
rd_all_ct_ps = torch.index_select(rd_all, 0, idx_ct_ps)
rt_all_ct_ps = torch.index_select(true, 0, idx_ct_ps)

''' 
rd_cs_ps_d1：重组的ps中属于i类别的feature vecs与非i类且出现类别的均值的差值（L1） 
rd_cs_ps_d0：重组的ps中属于i类别的feature vecs与i类别的均值的差值（L1） 
'''
rd_cs_ps_d0 = rd_cs_ps[None, :].t()
temp_0_min = torch.min(rd_cs_ps_d0)
temp_0_max = torch.max(rd_cs_ps_d0)
rd_cs_ps_d1 = torch.sub(rd_all_ct_ps, rd_cs_ps_d0)

''' rd_cs_ps_d1_ci：rd_cs_ps_d1 按类别分堆显示，且抛去0项 '''
for ci in range(ct.shape[0]):
        temp_ci = int(ct[ci, 0])
        if ci == 0:
                ci_idx = ci
        else:
                ci_idx = ci_idx + ct_idx[int(ct[ci - 1, 0])]
        names['rd_cs_ps_d1_c%s' % temp_ci] = rd_cs_ps_d1[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
        names['rd_cs_ps_d0_c%s' % temp_ci] = rd_cs_ps_d0[int(ci_idx): int(ci_idx + ct_idx[temp_ci])]
        names['rd_cs_ps_d1_c%s' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci][:, torch.arange(names['rd_cs_ps_d1_c%s' % temp_ci].size(1)) != ci]

        if ci == 0:
                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci])
                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci])
        else:
                temp_1_max = torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.max(names['rd_cs_ps_d1_c%s' % temp_ci]) > temp_1_max else temp_1_max
                temp_1_min = torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) if torch.min(names['rd_cs_ps_d1_c%s' % temp_ci]) < temp_1_min else temp_1_min


''' rd_cs_ps_d1_ci_p：rd_cs_ps_d1_ci BN化并sigmoid '''
for ci in range(ct.shape[0]):
        temp_ci = int(ct[ci, 0])
        if names['rd_cs_ps_d1_c%s' % temp_ci].size != 0:
                names['rd_cs_ps_d1_c%s_p' % temp_ci] = names['rd_cs_ps_d1_c%s' % temp_ci] - temp_1_max - 0.001
                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d1_c%s_p' % temp_ci], (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                temp_1_mid = torch.div(20, (0.002 + (temp_1_max + 0.001 - temp_1_min)))
                names['rd_cs_ps_d1_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d1_c%s_p' % temp_ci] + temp_1_mid)
                names['rd_cs_ps_d1_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d1_c%s_p' % temp_ci])

                names['rd_cs_ps_d0_c%s_p' % temp_ci] = names['rd_cs_ps_d0_c%s' % temp_ci] + 0.001
                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.div(names['rd_cs_ps_d0_c%s_p' % temp_ci], (0.002 + (temp_0_max + 0.001)))
                temp_0_mid = torch.div(10, (0.002 + (temp_0_max + 0.001)))
                names['rd_cs_ps_d0_c%s_p' % temp_ci] = 10 * (names['rd_cs_ps_d0_c%s_p' % temp_ci] - temp_0_mid)
                names['rd_cs_ps_d0_c%s_p' % temp_ci] = torch.sigmoid(names['rd_cs_ps_d0_c%s_p' % temp_ci])

        if ci == 0:
                rd_cs_ps_d1_ct_p = names['rd_cs_ps_d1_c%s_p' % temp_ci]
                rd_cs_ps_d0_ct_p = names['rd_cs_ps_d0_c%s_p' % temp_ci]
        else:
                rd_cs_ps_d1_ct_p = torch.cat((rd_cs_ps_d1_ct_p, names['rd_cs_ps_d1_c%s_p' % temp_ci]), 0)
                rd_cs_ps_d0_ct_p = torch.cat((rd_cs_ps_d0_ct_p, names['rd_cs_ps_d0_c%s_p' % temp_ci]), 0)

print('\n rd_cs_ps_d1_ct_p \n', rd_cs_ps_d1_ct_p)
print('\n rd_cs_ps_d0_ct_p \n', rd_cs_ps_d0_ct_p)







# bbb = [-2.3499,  0.1364, -2.3895, -2.4095, -0.0352,  0.4045, -2.6457, -3.1878,
#          -2.7262]
# aaa = [-3.0735, -2.5169,  1.7788, -2.3591, -2.3990, -2.7180, -2.4264,  3.2014,
#          -3.2332]
#
# aaaa = torch.tensor(aaa)
# bbbb = torch.tensor(bbb)
#
# xxxx = torch.sub(aaaa, bbbb).abs().sum()
# print("xxxx", xxxx)