from matplotlib import pyplot as plt
from matplotlib.pyplot import MultipleLocator
from mpl_toolkits.mplot3d import Axes3D
import cv2 as cv
import numpy as np
from openpyxl import Workbook
from openpyxl import load_workbook
import pandas as pd
import os

def mkdir(path):
    # 判断路径是否存在，若不存在则创建
    isExists = os.path.exists(path)
    if not isExists:
        # 创建目录操作函数
        os.makedirs(path)


def get_data(lines, epoch_num, epoch_size, check_cf_means):
    for i, line in enumerate(lines):
        line = line.strip()
        if line.startswith('*'):
            line = line.strip('*')
            line = line.strip()
            # print(line)
            epoch_num = 250 - epoch_size
            epoch_size = epoch_size - 1

        elif line.startswith('#'):
            line = line.rstrip('=')
            line = line.strip()
            line = line.strip('#')
            line = line.lstrip('[')
            line = line.rstrip(']')
            # print(line)

        else:
            a = i % 11
            row_n = (a - 2) if a > 1 else 0
            for j, data in enumerate(line.split()):
                check_cf_means[epoch_num, row_n, j] = data

    return check_cf_means

def show_data(wst_check_cf, out_path, zd=None):
    fig1 = plt.figure(figsize=(20, 20))
    ax1 = plt.axes(projection='3d')
    aaa = wst_check_cf.shape[0]
    bbb = wst_check_cf.shape[1]
    X = np.arange(0, aaa, 1)
    Y = np.arange(0, bbb, 1)
    w_t, w_id = np.meshgrid(X, Y)
    w_data = wst_check_cf[w_t, w_id]

    ax1.plot_surface(w_t, w_id, w_data, rstride=1, cstride=1, alpha=0.3, cmap='rainbow')  # 生成表面， alpha 用于控制透明度
    ax1.contour(w_t, w_id, w_data, zdir='z', offset=200, cmap="rainbow")  # 生成z方向投影，投到x-y平面
    # ax1.contour(w_t, w_id, w_data, zdir='x', offset=-6, cmap="rainbow")  # 生成x方向投影，投到y-z平面
    # ax1.contour(w_t, w_id, w_data, zdir='y', offset=6, cmap="rainbow")  # 生成y方向投影，投到x-z平面
    # ax1.contourf(w_t, w_id, w_data, zdir='y', offset=6, cmap="rainbow")   #生成y方向投影填充，投到x-z平面，contourf()函数

    # 设定坐标轴刻度
    x_major_locator = MultipleLocator(1)
    ax1.xaxis.set_major_locator(x_major_locator)
    y_major_locator = MultipleLocator(1)
    ax1.yaxis.set_major_locator(y_major_locator)

    # 设定坐标轴字体字号
    plt.tick_params(labelsize=5)
    labels = ax1.get_xticklabels() + ax1.get_yticklabels()
    [label.set_fontname('Times New Roman') for label in labels]

    # 设定坐标轴范围
    ax1.set_xlabel('t')
    ax1.set_xlim(0, aaa + 1)  # 拉开坐标轴范围显示投影
    ax1.set_ylabel('id')
    ax1.set_ylim(0, bbb + 1)
    ax1.set_zlabel('data')
    ax1.set_zlim(-zd, zd)

    plt.savefig(fname=out_path, figsize=[20, 20])

    # # plt.legend()
    # plt.show()

def write_excel(data_01_exl, sheet_01):
    [r, c] = data_01_exl.shape  # h为行数，l为列数
    for i in range(r):
        row = []
        for j in range(c):
            row.append(data_01_exl[i, j])
        sheet_01.append(row)

def data_to_excel(data_01, sheet_01, add_row):
    epoch_n = data_01.shape[0]
    nc_n = data_01.shape[1]
    wstnc_n = data_01.shape[2]

    for i in range(epoch_n):
        if i == 0:
            data_01_exl = np.row_stack((data_01[i, :, :], add_row))
        else:
            data_01_exl = np.append(data_01_exl, np.row_stack((data_01[i, :, :], add_row)), axis=0)
    print("cf_means_exl =>", data_01_exl)

    write_excel(data_01_exl, sheet_01)

def data_to_excel_2d(data_01, sheet_01, add_row):
    write_excel(data_01, sheet_01)

def save_to_excel(excel_out_path, data, add_row, nd='3d'):
    df = pd.DataFrame()                     # 创建excel文档，只是一个圆括号的话说明创建的数据表是空的
    df.to_excel(excel_out_path)             # 调用to_excel 方法
    # print('Done!')

    wb = Workbook()
    print(wb.get_sheet_names())             # 提供一个默认名叫Sheet的表，office2016下新建提供默认Sheet1

    sheet_01 = wb.get_sheet_by_name('Sheet')
    sheet_01.title = 'cf_means_all'           # 直接赋值就可以改工作表的名称
    if nd == '3d':
        data_to_excel(data, sheet_01, add_row)
    elif nd == '2d':
        data_to_excel_2d(data, sheet_01, add_row)

    wb.save(excel_out_path)


if __name__ == '__main__':

    fd = 'WST_CF'
    mkdir(fd)

    '''================================ 处理文件 + 绘图 ================================'''
    # epoch_num = 0
    # epoch_size = 300
    # check_num = 0
    epoch_num = 0
    epoch_size = 250
    class_num = 0
    wst_num = 0

    '''*********** 处理 wst_check_w 文件 ***********'''
    path = "WST_cf_means.txt"
    out_path_name = fd + "/WST_cf_means"
    names = locals()

    with open(path, 'r') as f:
        lines = f.readlines()
        # print(lines)

    wst_cf_means = np.zeros((250, 9, 81))
    wst_cf_means = get_data(lines, epoch_num, epoch_size, wst_cf_means)
    for i in range(9):
        names['cf_means_c%s' % i] = wst_cf_means[:, i, :]
        out_path = out_path_name + '_c' + str(i) + '.png'
        show_data(names['cf_means_c%s' % i], out_path, zd=20)
        print('cf_means_c%s' % i)

    temp_n = epoch_size / 16
    for j in range(16):
        nj = int(np.floor(j * temp_n))
        names['cf_means_epoch%s' % nj] = wst_cf_means[nj, ...]
        out_path = out_path_name + '_epoch' + str(nj) + '.png'
        show_data(names['cf_means_epoch%s' % nj], out_path, zd=20)
        print('cf_means_epoch%s' % nj)


    '''================================ 转存为excel 供origin绘图 ================================'''
    add_row = np.zeros(81)

    '''*********** 文件转存为excel文件 ***********'''
    excel_out_path_all = fd + "/WST_cf_means_all.xlsx"
    save_to_excel(excel_out_path_all, wst_cf_means, add_row)

    '''*********** 文件转存为excel文件 ***********'''
    excel_out_path_c = fd + "/WST_cf_means_c"
    for i in range(9):
        names['cf_means_c%s' % i] = wst_cf_means[:, i, :]
        out_path = excel_out_path_c + str(i) + '.xlsx'
        print('Excel: cf_means_c%s' % i)
        save_to_excel(out_path, names['cf_means_c%s' % i], add_row, nd='2d')

    temp_nn = epoch_size / 16
    wst_cf_means_some = np.zeros((16, 9, 81))
    for j in range(16):
        nj = int(np.floor(j * temp_nn))
        names['cf_means_epoch%s' % nj] = wst_cf_means[nj, ...]
        wst_cf_means_some[j, ...] = names['cf_means_epoch%s' % nj]
    print('Excel: wst_cf_means_some')
    out_path_some = fd + "/WST_cf_means_some.xlsx"
    save_to_excel(out_path_some, wst_cf_means_some, add_row)


