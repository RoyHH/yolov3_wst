import torch
import torch.nn as nn
import numpy as np

a = [[-0.31808,  0.49600, -0.12628,  0.27902, -4.52833, -2.29883, -2.39629, -2.06249, -2.65240, -2.85966, -2.33141, -2.40722, -2.25831, -2.42140],
        [ 0.35091,  0.44638, -0.14777, -0.16970, -4.19774, -2.45219, -2.50819, -2.05217, -3.04041, -2.63662, -2.20844, -2.54585, -1.96923, -2.29641],
        [ 0.39894,  0.69730, -0.56890,  0.99374, -4.19221, -2.31267, -2.20399, -1.69904, -2.72543, -2.38454, -2.65685, -3.07336, -2.23358, -2.30209],
        [ 0.82628,  0.94285, -0.54095,  0.89705, -3.76685, -1.98006, -1.92894, -2.25423, -2.96363, -3.23576, -2.08876, -3.06925, -1.86419, -2.32774],
        [ 0.10969,  1.23654, -0.06494,  0.10677, -4.53736, -2.18854, -1.87136, -2.24761, -2.61594, -3.46782, -2.36368, -2.50071, -1.83077, -2.77924],
        [-2.06852, -0.74637,  0.25547,  1.03924, -5.44098, -2.75003, -3.37161, -2.82438, -3.39337, -1.64945, -2.56177, -3.06094, -3.51442, -1.51761]]

b = [-2.29883, -2.39629, -2.06249, -2.65240, -2.85966, -2.33141, -2.40722, -2.25831, -2.42140]
c = [-2.45219, -2.50819, -2.05217, -3.04041, -2.63662, -2.20844, -2.54585, -1.96923, -2.29641]
bb = torch.tensor(b)
cc = torch.tensor(c)
d = torch.cosine_similarity(bb, cc, dim=0)
print("d = ", d)

b = np.array(b)
c = np.array(c)
e = b * c
print("e = ", e)
print(2.29883 * 2.45219)

fb = np.linalg.norm(b)
fc = np.linalg.norm(c)
print("fb = ", fb)
print("fc = ", fc)
print("f = ", fb * fc)

e = e.sum()
print("e.sum() = ", e)
g = e / (fb * fc)
print("g = ", g)

# pre = torch.tensor(a)
# wst_d_ps = pre.clone()
#
# wst_temp = wst_d_ps[:, 5:]
# print('wst_temp', wst_temp)
#
# wst_a = wst_temp.unsqueeze(dim=1)
# wst_b = wst_temp.unsqueeze(dim=0)
# print('wst_a', wst_a)
# print('wst_b', wst_b)
#
# wst_d_temp = wst_a * wst_b
# print('wst_d_temp', wst_d_temp)
#
# wst_d_temp = wst_d_temp.sum(dim=2)
# print('wst_d_temp', wst_d_temp)
#
# wst_l2 = torch.norm(wst_temp, dim=1, p=2)
# print('wst_l2', wst_l2)
#
# wst_l2_a = wst_l2.unsqueeze(dim=1)
# wst_l2_b = wst_l2.unsqueeze(dim=0)
# print('wst_l2_a', wst_l2_a)
# print('wst_l2_b', wst_l2_b)
#
# wst_l2_temp = wst_l2_a * wst_l2_b
# print('wst_l2_temp', wst_l2_temp)
#
# wst_d_temp = wst_d_temp / wst_l2_temp
# print('wst_d_temp', wst_d_temp)
#
# for i in range(wst_d_temp.shape[0]):
#         if i == 0:
#                 wst_d = wst_d_temp[0, i+1:]
#         else:
#                 wst_d = torch.cat((wst_d, wst_d_temp[i, i+1:]), 0)
# print('wst_d', wst_d)


pred = torch.tensor(a)
wst_d_ps = pred.clone()



wst_d_a = wst_d_ps[:, 5:].unsqueeze(dim=1)
wst_d_b = wst_d_ps[:, 5:].unsqueeze(dim=0)
wst_d_temp = wst_d_a * wst_d_b
wst_d_temp = wst_d_temp.sum(dim=2)

wst_l2 = torch.norm(wst_d_ps[:, 5:], dim=1, p=2)
wst_l2_a = wst_l2.unsqueeze(dim=1)
wst_l2_b = wst_l2.unsqueeze(dim=0)
wst_l2_temp = wst_l2_a * wst_l2_b

wst_temp = wst_d_temp / wst_l2_temp
print('wst_temp', wst_temp)

for i in range(wst_temp.shape[0]):
        if i == 0:
                wst_d = wst_temp[0, i+1:]
        else:
                wst_d = torch.cat((wst_d, wst_temp[i, i+1:]), 0)
print('wst_d', wst_d)

wst_pre = 1 - wst_d