### **1.尝试计划**
- [ ] YOLO体系下的尝试
    - [ ] 微调Trick，超参的优化选择，看效果
    - [ ] 未训练类别的识别能力，看效果
- [ ] 其它体系下的尝试
    - [ ] 使用PP-YOLO，精简且高效版的YOLO，使用ResNet-vd，作为ResNet的过度架构，看效果
    - [ ] 使用ResNet体系
        - [ ] 使用Faster RCNN，看效果
        - [ ] 使用Mask RCNN，看效果
        - [ ] 结合目标分割方法，看效果
- [ ] 整体
    - [ ] 小批量数据集下的梯度寻优策略优化，看效果
    - [ ] 多任务目标优化，看效果
***

### **2.待解决问题**
- 思考建立类别间的关联
> 目前采用多分类器的方式处理，随着epoch更替做指定类别争强权重指定训练。
> 未来希望找到更软性的优化方式，且能进行数学层面上的理论推导
- 多任务目标优化方法
> 目前使用联合分布的方式处理问题，且用的权重比较硬，现在的效果不明显且使用场景还没有总结出来，待解决。
***

### **3.正在做**
- CF_means各类别聚类的簇心分析
> 目前我已经把对比学习中的各类别簇心都已经调取出来了，正在寻找好的处理，分析，可视化方式。
- 未训练类别的识别能力实验
> 数据集搭建完毕，正规化实验思路。
***

### **4.使用记录**
- v1、v3模式
> 都包含 L1、L2、Cosine、Cosdis 模块，应用在 WST Loss 计算模块和 WST test 评价模块中
- WST Head框架
> 包含：Single, Same, None
>> 1. None: 原始YOLO
>> 2. Same: WST Head 与 yolo Head 共用
>> 3. Single: WST Head 独立存在，不与 yolo Head 共用
- WST Head框架和 v1、v3模式 切换
```
train.py: WST_module_name(p51)
test.py: WST_module_name(p14)、 test_module_name(p23)
utils.py: WST_module_name(p35)、wst_loss(p47)、 loss_module_name(p54) 、 measure(p68)
models.py: WST_module_name(p7)
```
- BN 切换
> 包含：Done, None
>> 1. Done: 在做度量前 Feature maps 做 BN 操作
>> 2. None: 在做度量前 Feature maps 不做 BN 操作
```
train.py: BN_WST_module(p57)
utils.py: BN_WST_module(p66)
```
- OHEM 切换
> 包含：Done, None
>> 1. Done: 在做Loss前做 OHEM 难例硬筛选操作
>> 2. None: 在做Loss前不做 OHEM 难例硬筛选操作
```
utils.py: OHEM_module(p74)
```
- WST Metric 模式切换
> 包含：Normal, better
>> 1. Normal: 包含标准的precise，recall和acc，但是存在一些主观不确定因素，导致这部分不能正确反映其性能变化，待解决
>> 2. better: 只包含异类间的wronge和right在全样本中的占比，能够客观准确反映对比学习性能
```
train.py: WST_Metric_module(p68)
test.py: WST_Metric_module(p37)
```
***

### **5.随笔记录**
- na: number of anchor
> every grid cell has na anchor boxes
- nt: number of target
> 一个batch-size被3层yolo layers 按照grid size划分后，得到的目标target（target是真实样本目标的子目标）
> 
> [iamge编号, class, x1, x2, y1, y2]
- nb: number of targets per one of yolo layer
> 当前yolo layer上target的数量
- b, a, gj, gi: image, anchor, gridy, gridx 
> image表示的是batch-size中第几张图在当前yolo layer上有target
- ps = pi[b, a, gj, gi]
> pi.shape是[batch-size, 每个grid cell的anchor box数量, grid size, grid size, feature-vec.lenght]
>
> 假设当前pi.shape是[6, 3, 16, 16, 14], 那么ps.shape可能是[8, 14]
>
> 即ps.shape表示的是[基于batch-size中当前yolo layer上可以匹配到target的anchor box数量, feature-vec.lenght]
- pbox、tbox
> pbox是anchor box的标注[x1, y1, w, h]
>
> tbox是target box的坐标[x1, y1, x2, y2]

- some
> [some_description](./tools/some_description.jpg)
***

### **6.目前想法**
- OHEM的硬性难例筛选效果不好，弃 

